<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php wp_head(); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="apple-touch-icon" sizes="57x57" href="<?= get_template_directory_uri() . '/assets/images/apple-icon-57x57.png';?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= get_template_directory_uri() . '/assets/images/apple-icon-60x60.png';?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= get_template_directory_uri() . '/assets/images/apple-icon-72x72.png';?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= get_template_directory_uri() . '/assets/images/apple-icon-76x76.png';?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= get_template_directory_uri() . '/assets/images/apple-icon-114x114.png';?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= get_template_directory_uri() . '/assets/images/apple-icon-120x120.png';?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= get_template_directory_uri() . '/assets/images/apple-icon-144x144.png';?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= get_template_directory_uri() . '/assets/images/apple-icon-152x152.png';?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri() . '/assets/images/apple-icon-180x180.png';?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= get_template_directory_uri() . '/assets/images/android-icon-192x192.png';?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri() . '/assets/images/favicon-32x32.png';?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= get_template_directory_uri() . '/assets/images/favicon-96x96.png';?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= get_template_directory_uri() . '/assets/images/favicon-16x16.png';?>">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-74045722-1', 'auto');
        ga('send', 'pageview');

    </script>
    <!-- End Google Analytics -->

    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<?php
$wp_customize = new Webqam_Customize();
$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
if ( $myaccount_page_id ) {
    $myaccount_page_url = get_permalink( $myaccount_page_id );
}
global $woocommerce;
$cart_url = $woocommerce->cart->get_cart_url();
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<body <?php body_class(); ?>>

<header class="site-header" role="banner">
    <div class="main-site-header grid">
        <div class="site-title col-3-12">
            <div class="site-logo">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?= bloginfo('name'); ?>">
                    <img width="245" height="61" src="<?= get_template_directory_uri() . '/assets/images/alter-logo.png' ?>" alt="<?= bloginfo('name'); ?>"/>
                </a>
            </div>
            <h2><?= get_bloginfo('description') ?></h2>
        </div>
        <div class="site-menu col-7-12">
            <nav class="site-navigation primary-navigation wrapper" role="navigation">
                <?php wp_nav_menu( array( 'menu' => 'header', 'menu_class' => 'nav-menu' ) ); ?>
            </nav>
            <nav class="site-navigation-mobile primary-navigation wrapper" role="navigation">
                <?php wp_nav_menu( array( 'menu' => 'mobile', 'menu_class' => 'nav-menu' ) ); ?>
            </nav>
        </div>
        <div class="site-menu-top col-2-12">
            <nav id="top-navigation" class="top-navigation" role="navigation">
                <ul id="menu-top" class="menu">
                    <li class="my-account"><a href="<?= $myaccount_page_url; ?>">Mon compte</a></li>
                    <li class="my-cart">
                        <a href="<?= $cart_url; ?>">
                            Mon panier
                        </a>
                <span class="header-amount">
                <?= sprintf (_n( '%d ', '%d', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count );?>
                </span>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>