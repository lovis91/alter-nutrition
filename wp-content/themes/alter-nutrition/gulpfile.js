var dir, requireDir;

global.path = {
    server: '/',
    scss: "assets/sass/",
    img: "assets/images/",
    sprite: "assets/images/icons/",
    js: "assets/js/source/",
    plugins: "assets/js/source/vendor/",
    css: "assets/css/"
};

global.browser_support = ["ie >= 9", "ie_mob >= 10", "ff >= 30", "chrome >= 34", "safari >= 7", "opera >= 23", "ios >= 7", "android >= 4.4", "bb >= 10"];

requireDir = require('require-dir');

dir = requireDir('./tasks');
