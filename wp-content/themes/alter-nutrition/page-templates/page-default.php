<?php
/*
Template Name: Page-Default
*/

get_header(); ?>
<div class="page-default grid">
    <?php
    get_header();
    if (have_posts()) :
        while (have_posts()) :
            the_post();
            ?>
            <div class="page-title col-1-1">
                <h1><?= the_title(); ?></h1>
            </div>
            <div class="page-content col-1-1">
                <?= the_content(); ?>
            </div>
            <?php
        endwhile;
    endif;
    ?>
</div>
<?php
get_footer();
?>
