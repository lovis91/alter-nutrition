<?php
/*
Template Name: Page-Whey
*/
$wq = Webqam::getInstance();
$wp_query = $wq->getWheyBio();
$content = get_field('content');

$cowImg = get_field('cow-img');
$cowTitle = get_field('cow-title');
$cowDesc = get_field('cow-desc');

$milkImg = get_field('milk-img');
$milkTitle = get_field('milk-title');
$milkDesc = get_field('milk-desc');

$filtrationImg = get_field('filtration-img');
$filtrationTitle = get_field('filtration-title');
$filtrationDesc = get_field('filtration-desc');

$confectionImg = get_field('confection-img');
$confectionTitle = get_field('confection-title');
$confectionDesc = get_field('confection-desc');

get_header(); ?>
<div class="page-whey">
    <div class="whey-banner">
        <div class="grid">
            <h1><?= the_title(); ?></h1>
        </div>
        <div class="grid">
            <div class="col-1-4">
                <div class="whey-banner-img cow">
                    <img src="<?= $cowImg['url']; ?>" alt="<?= $cowTitle ?>" />
                </div>
                <div class="whey-banner-title">
                    <h2><?= $cowTitle; ?></h2>
                </div>
                <div class="whey-banner-desc">
                    <?= $cowDesc; ?>
                </div>
            </div>
            <div class="col-1-4">
                <div class="whey-banner-img milk">
                    <img src="<?= $milkImg['url']; ?>" alt="<?= $milkTitle ?>" />
                </div>
                <div class="whey-banner-title">
                    <h2><?= $milkTitle; ?></h2>
                </div>
                <div class="whey-banner-desc">
                    <?= $milkDesc; ?>
                </div>
            </div>
            <div class="col-1-4">
                <div class="whey-banner-img filtration">
                    <img src="<?= $filtrationImg['url']; ?>" alt="<?= $filtrationTitle ?>" />
                </div>
                <div class="whey-banner-title">
                    <h2><?= $filtrationTitle; ?></h2>
                </div>
                <div class="whey-banner-desc">
                    <?= $filtrationDesc; ?>
                </div>
            </div>
            <div class="col-1-4">
                <div class="whey-banner-img confection">
                    <img src="<?= $confectionImg['url']; ?>" alt="<?= $confectionTitle ?>" />
                </div>
                <div class="whey-banner-title">
                    <h2><?= $confectionTitle; ?></h2>
                </div>
                <div class="whey-banner-desc">
                    <?= $confectionDesc; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="grid whey-content">
        <?= $content; ?>
    </div>
    <div class="grid">
        <?php
        query_posts(array('post_type' => 'avantage','orderby' => 'menu_order'));
        if(have_posts()) :
            $i = 0;
            while(have_posts()) : the_post();
                $i++;
                get_template_part( 'content/content', 'whey-avantage' );
                if (($i % 2) == 0) {
                    ?>
                    <div style="clear: both;"></div>
                <?php
                }
            endwhile;
        endif; wp_reset_query(); ?>
    </div>
</div>
<?php
get_footer();
?>
