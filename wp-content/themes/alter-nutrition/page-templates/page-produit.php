<?php
/*
Template Name: Page-Produit
*/

get_header();
$count = 0;
global $product;
query_posts(array('post_type' => 'product')); if(have_posts()) : while(have_posts()) : the_post();
    if (get_the_ID() !== $product->id) {
        $count++;
    }
endwhile; endif; wp_reset_query();
?>

<div class="content">
    <?= do_shortcode('[product_page id="13"]'); ?>
</div>
<?php
if ($count > 0) {
    ?>
    <div id="related-slider" class="flex-slider">
        <h2 class="related">Nos produits et accessoires</h2>
        <ul class="slides related-slides">
            <?php
            query_posts(array('post_type' => 'product')); if(have_posts()) : while(have_posts()) : the_post();
                if (get_the_ID() !== $product->id) {
                    get_template_part( 'content/content', 'related-slider' );
                }
            endwhile; endif; wp_reset_query(); ?>
        </ul>
    </div>
    <?php
}
?>

<?php
get_footer();
?>