<?php
/*
Template Name: Page-Home
*/
$productPath = get_permalink (88);
$blogPath = get_permalink (7);
get_header(); ?>
    <section id="homepage" class="page visible">
        <div id="main-slider">
            <div class="flexslider flexslider-main">
                <ul class="slides main-slides">
                    <?php
                    query_posts(array('post_type' => 'slide','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                        get_template_part( 'content/content', 'home-slider' );
                    endwhile; endif; wp_reset_query(); ?>
                </ul>
            </div>
        </div>
        <div id="skill-slider">
            <ul class="grid grid-pad skill-slides">
                <?php
                query_posts(array('post_type' => 'skill','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                    get_template_part( 'content/content', 'skill-slider' );
                endwhile; endif; wp_reset_query(); ?>
            </ul>
        </div>
        <div id="home-category">
            <div class="grid grid-pad">
                <?php
                query_posts(array('post_type' => 'home-bloc','orderby' => 'menu_order')); if(have_posts()) : while(have_posts()) : the_post();
                    get_template_part( 'content/content', 'home-bloc' );
                endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>
    </section>
<?php
get_footer();
?>