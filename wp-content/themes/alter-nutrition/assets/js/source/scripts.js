// Declare namespace
var Webqam = Webqam || {};

Webqam.General = function() {
};

Webqam.General.prototype = {
    init: function() {
        var that = this;
        that.homeSlider();
        that.productSlider();
        that.relatedSlider();
        that.fixedHeader();
        that.fancyBox();
        that.slickNav();
    },
    homeSlider: function() {

        $('.flexslider-main').flexslider({
            animation: "slide",
            useCSS: false, /* Chrome fix*/
            slideshow: true,
            smoothHeight: true,
            animationSpeed: 1500,
            direction: "horizontal"
        });
    },
    productSlider: function(){

        $('#product-carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 50,
            itemMargin: 15,
            asNavFor: '#product-slider'
        });

        $('#product-slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#product-carousel"
        });

    },
    relatedSlider: function(){


        $('#related-slider').flexslider({
            animation: "slide",
            animationLoop: false,
            itemWidth: 400,
            itemMargin: 0,
            maxItems: 1
        });

    },
    fixedHeader: function() {
        $(window).scroll(function(){
            var sticky = $('header'),
                scroll = $(window).scrollTop();

            if (scroll >= 100) sticky.addClass('fixed');
            else sticky.removeClass('fixed');
        });
    },
    fancyBox: function (){
        $('a.product-image').fancybox();
    },
    slickNav: function (){
        $('#menu-mobile').slicknav({
            label: '',
            duration: 200,
            duplicate: false,
            prependTo: $('.site-menu'),
            openedSymbol: "&#9660;",
            closedSymbol: "&#9658;"
        });

        $('body').bind('touchstart', function() {}); //get hovers on touch
    }


};

$( document ).on( 'click', 'a.post-cat-link', do_ajax_request );

function do_ajax_request() {
    var slug = $( this ).attr( 'data-cat' );
    if (slug == 'recette') {
        $('.actus-child-cat').removeClass('visible');
        $('.recette-child-cat').addClass('visible');
    }
    else if (slug == 'actu'){
        $('.recette-child-cat').removeClass('visible');
        $('.actus-child-cat').addClass('visible');
    }
    $('.category-title a').removeClass('active');


    $(this).addClass('active');
    doAjaxRequest(slug);
}

function doAjaxRequest(slug){
    var ajaxurl = '/wp-admin/admin-ajax.php';
    var $content = $('.post-container');
    jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        data: {'action':'do_ajax',
            'fn': 'articleByCat',
            'id':slug
        },
        beforeSend: function() {
            $content.html();
            $content.empty();
            $content.append('<div id="loader"></div>');
        },
        success: function(response) {
            $content.find('#loader').remove();
            $content.append(response);
        }
    });
}

$(document).ready(function() {
    var g = new Webqam.General();

    g.init();
});
