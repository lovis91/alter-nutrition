<?php
get_header();
$catRecette = get_category_by_slug('recette');
$catActus = get_category_by_slug('actu');
?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <section class="article">
        <div class="grid category-title">
            <div class="col-1-1 category-recette">
                <a href="<?= get_permalink( get_option( 'page_for_posts' ) ) ?>" class="post-cat-link active" ><?php _e('Retour au blog') ?></a>
            </div>
        </div>
        <div class="grid">
            <div class="col-1-1 article-header">
                <?php the_post_thumbnail('big', array('class' => 'alignleft')); ?>
            </div>
        </div>
        <div class="grid">
            <div class="col-1-1 article-content">
                <h1>
                    <?= the_title(); ?>
                </h1>
            </div>
            <div class="col-1-1">
                <div class="content">
                    <?= the_content(); ?>
                </div>
            </div>
        </div>
    </section>

    <?php
endwhile;
endif;
wp_reset_query();
get_footer();
