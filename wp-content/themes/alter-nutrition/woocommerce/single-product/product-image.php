<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;
$images = $product->get_gallery_attachment_ids();

?>
<div class="images col-1-3">

	<div id="product-slider" class="product-flexslider">
		<ul class="slides">
			<?php

			foreach( $images as $image )
			{
				?>
				<li>
					<a class="product-image" href="<?= wp_get_attachment_url( $image ); ?>">
						<img src="<?= wp_get_attachment_url( $image ); ?>" />
					</a>
				</li>
			<?php
			}
			?>
		</ul>
	</div>
	<div id="product-carousel" class="product-flexslider">
		<ul class="slides">
			<?php

			foreach( $images as $image )
			{
				?>
				<li>
					<img src="<?= wp_get_attachment_url( $image ); ?>" />
				</li>
				<?php
			}
			?>
		</ul>
	</div>
</div>
