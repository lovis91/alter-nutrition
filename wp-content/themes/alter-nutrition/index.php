<?php
get_header();
$catRecette = get_category_by_slug('recette');
$catActus = get_category_by_slug('actu');

$RecetteArgs = array('parent' => $catRecette->cat_ID);
$ActuArgs = array('parent' => $catActus->cat_ID);
$RecetteCat = get_categories( $RecetteArgs );
$ActuCat = get_categories( $ActuArgs );

?>
<section id="blog" class="page">
    <div class="grid category-title">
        <div class="col-1-2 category-recette">
            <a class="post-cat-link active" data-cat="<?= $catRecette->slug ?>" target="_blank"><?= $catRecette->name ?></a>
        </div>
        <div class="col-1-2 category-actu">
            <a class="post-cat-link" data-cat="<?= $catActus->slug ?>" target="_blank"><?= $catActus->name ?></a>
        </div>
    </div>
    <div class="grid">
        <div class="col-1-1 category-sub-list recette-child-cat visible">
            <?php
            foreach ($RecetteCat as $cat) {
                ?>
                <a class="post-cat-link" data-cat="<?= $cat->slug ?>" target="_blank"><?= $cat->name ?></a>
            <?php
            }
            ?>
        </div>
        <div class="col-1-1 category-sub-list actus-child-cat">
            <?php
            foreach ($ActuCat as $cat) {
                ?>
                <a class="post-cat-link" data-cat="<?= $cat->slug ?>" target="_blank"><?= $cat->name ?></a>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="post-container">
        <?php
        $i = 0;
        query_posts(array('post_type' => 'post','category_name' => 'recette'));
        $count = count(query_posts(array('post_type' => 'post','category_name' => 'recette')));
        if(have_posts()) :

            while(have_posts()) : the_post();
                if ($i == 0 || $i == 1) {
                    ?>
                    <div class="grid">
                    <?php
                }
                if ($i == 0) {
                    get_template_part( 'content/content', 'preview-main-post' );
                }
                else {
                    get_template_part( 'content/content', 'preview-post' );
                }
            if ( $i == 0 || $i == $count-1 ) {
                ?>
                </div>
                <?php
            }
            $i++;
        endwhile;
        endif;
        wp_reset_query(); ?>
    </div>

</section>
<?php
get_footer();
?>
