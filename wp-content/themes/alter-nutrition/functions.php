<?php
require_once realpath(__DIR__ . '/includes/class/Singleton.php');
require_once realpath(__DIR__ . '/includes/helpers.php');
require_once realpath(__DIR__ . '/includes/options.php');

/**
 * Webqam singleton.
 *
 * @author Olivier Barou <obarou@webqam.fr>
 */
class Webqam extends Singleton {

    const PACKAGE_VERSION = '0.0.1';

    /**
     * @var int
     */
    protected $cacheVersion;

    /**
     * Constructor.
     */
    protected function __construct() {

        $this->cacheVersion = LOCAL_WORK || IS_PREPRODUCTION ? time() : self::PACKAGE_VERSION;
        $this->tplDir = get_template_directory_uri();

    }

    /**
     * @return Webqam
     *
     * Overrided to get autocompletion in IDE, nothing else to do here
     */
    public static function getInstance() {
        return parent::getInstance();
    }

    /**
     * Initialization.
     */
    public function init() {

        load_theme_textdomain( 'alter-nutrition', get_template_directory() . '/languages' );

        $this->actions();
        $this->filters();
        $this->menus();
        $this->images();
        $this->defines();
        $this->singleTemplate();
        $this->initCustomizations();
    }

    /**
     * Add the custom field for the theme
     */
    protected function initCustomizations() {

        require_once realpath(__DIR__ . '/includes/class/Webqam_customize.php');
        // Setup the Theme Customizer settings and controls...
        add_action( 'customize_register' , array( 'Webqam_Customize' , 'register' ) );

    }

    /**
     * @return $this
     */
    protected function actions() {

        if (!is_admin()) {
            add_action('wp_enqueue_scripts', array($this, 'loadStyles'));
            add_action('wp_enqueue_scripts', array($this, 'loadScripts'));
        }

        // Add AJAX actions
        add_action('wp_ajax_nopriv_do_ajax', array($this, 'getArticleByCat'));
        add_action('wp_ajax_do_ajax', array($this, 'getArticleByCat'));

        //  Remove Wordpress meta informations from header
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        //  Remove extra feed links from header
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'feed_links_extra', 3);
        // Woocommerce
        remove_action( 'woocommerce_before_main_content', array($this, 'woocommerce_output_content_wrapper', 10));
        remove_action( 'woocommerce_after_main_content', array($this, 'woocommerce_output_content_wrapper_end', 10));
        add_action('woocommerce_before_main_content', array($this, 'my_theme_wrapper_start', 10));
        add_action('woocommerce_after_main_content', array($this, 'my_theme_wrapper_end', '10'));
        add_action( 'after_setup_theme', array($this, 'woocommerce_support'));

        return $this;

    }

    /**
     * @return array
     */
    public function getArticleByCat(){

        global $post;

        $slug = $_POST['id'];
        $args = array(
            'post_type' =>'post',
            'category_name' => $slug
        );

        $ajax_query = new WP_Query($args);
        $i = 0;
        $count = $ajax_query->found_posts;
        if ( $ajax_query->have_posts() ) :

            while($ajax_query->have_posts()) : $ajax_query->the_post();
                if ($i == 0 || $i == 1) {
                    ?>
                    <div class="grid">
                    <?php
                }
                if ($i == 0) {
                    get_template_part( 'content/content', 'preview-main-post' );
                }
                else {
                    get_template_part( 'content/content', 'preview-post' );
                }
                if ( $i == 0 || $i == $count-1 ) {
                    ?>
                    </div>
                    <?php
                }
                $i++;
            endwhile;
        endif;

        die();
    }

    function woocommerce_support() {
        add_theme_support( 'woocommerce' );
    }
    function my_theme_wrapper_start() {
        echo '<section id="main">';
    }

    function my_theme_wrapper_end() {
        echo '</section>';
    }

    protected function filters() {

        /**
         * Adds "start_in" argument.
         *
         * only submenus of this id will be displayed.
         */
        add_filter("wp_nav_menu_objects", function($sortedMenuItems, $args) {

            if(!isset($args->start_in))
                return $sortedMenuItems;

            $objectByitems = array();

            // In parent mode, start at current parent menu item
            if ($args->start_in == "parent") {
                foreach( $sortedMenuItems as $item ) {

                    $objectByitems[$item->ID] = $item->object_id;

                    // If it's curent item
                    if ($item->current)
                        $args->start_in =  $item->menu_item_parent;

                }

                // If there is no "current"
                if ($args->start_in == "parent")
                    return null;

                $args->start_in = $objectByitems[$args->start_in];
            }


            $menuItemParents = array();
            foreach( $sortedMenuItems as $key => $item ) {
                // init menu_item_parents
                if( $item->object_id == (int)$args->start_in )
                    $menuItemParents[] = $item->ID;

                if( in_array($item->menu_item_parent, $menuItemParents) ) {
                    // part of sub-tree: keep!
                    $menuItemParents[] = $item->ID;
                } else {
                    // not part of sub-tree: away with it!
                    unset($sortedMenuItems[$key]);
                }
            }

            return $sortedMenuItems;

        },10,2);


        // These 2 hooks removes domain on images src when they are uploaded/added to an article.
        // This makes transfers/production/domain changes a lot easier.
        add_filter('wp_handle_upload', function($file){

            // removes absolute prefix from URL
            $file["url"] = str_replace(get_bloginfo('url'), "", $file["url"]);

            return $file;
        });
        add_filter('image_send_to_editor', function($html){

            // removes absolute prefix from image src and link
            $html = str_replace(get_bloginfo('url'), "", $html);

            return $html;
        });

    }

    /**
     * Register basic menus.
     *
     * @return $this
     */
    protected function menus() {
        add_theme_support('menus');
        register_nav_menu('main-nav', 'Navigation principale');
        register_nav_menu('footer-nav', 'Footer');

        return $this;
    }

    /**
     * Specifies thumbnail sizes.
     *
     * @return $this
     */
    protected function images() {
        //  Enable thumbnails
        add_theme_support('post-thumbnails');

        //  Specify image formats
        // add_image_size("format", 720, 250);

        return $this;

    }

    /**
     * Define here all constants you need.
     *
     * @return $this
     */
    protected function defines() {

        // Define directory for single page
        defined('SINGLE_PATH') || define('SINGLE_PATH', TEMPLATEPATH . '/single-templates');

        return $this;
    }

    /**
     * Define directory for single page
     *
     * @return void
     */
    protected function singleTemplate() {

        add_action( 'single_template', function($single_template) {
            global $post;

            if(file_exists(SINGLE_PATH . '/single-' . $post->post_type . '.php'))
                $single_template = SINGLE_PATH . '/single-' . $post->post_type . '.php';

            return $single_template;

        } );

    }

    /**
     * Register and enqueue scripts.
     *
     * @return $this
     */
    public function loadScripts()
    {

        wp_register_script('plugins', "$this->tplDir/assets/js/build/plugins.min.js", array('jquery'), null);
        wp_register_script('main', "$this->tplDir/assets/js/build/scripts.min.js", array('jquery'), null);

        //  Enqueue
        wp_enqueue_script('plugins', '$this->tplDir/assets/js/plugins.min.js', '', '', true);
        wp_enqueue_script('main', '$this->tplDir/assets/js/main.min.js', '', '', true);

        return $this;

    }


    /**
     * Register and enqueue styles.
     *
     * @return $this
     */
    public function loadStyles() {

        //  Register
        wp_register_style('main', "$this->tplDir/assets/css/front.css", array(), $this->cacheVersion);

        //  Enqueue
        wp_enqueue_style('main');

        return $this;

    }

    /**
     * Return a template with the given vars previously declared.
     *
     * @param string $template
     * @param array $vars
     * @return string
     */
    public function getTemplatePartWithVars($template, $vars = array()) {

        if (!empty($vars))
            extract($vars);

        ob_start();
        include(locate_template($template));
        return ob_get_clean();
    }

    /**
     * Return blog articles
     *
     * @return array
     */
    public function getWheyBio()
    {

        $blog_query_args = array(
            'post_type' => "fabrication",
            'order' => "menu",
        );

        return new WP_Query($blog_query_args);
    }

    /**
     * Return footer parts
     *
     * @return array
     */
    public function getFooterParts()
    {

        $blog_query_args = array(
            'post_type' => "footer",
            'order' => "menu",
        );

        return new WP_Query($blog_query_args);
    }


}

Webqam::getInstance()->init();

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
add_filter( 'woocommerce_price_trim_zeros', function() { return false; } );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}
