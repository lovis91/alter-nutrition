var $, browser_support, gulp, path, bulkSass;

gulp = require("gulp");

$ = require("gulp-load-plugins")();

bulkSass = require('gulp-sass-bulk-import');

path = global.path;

browser_support = global.browser_support;

gulp.task('sass', function() {
    return gulp.src(path.scss + 'front.scss')
        .pipe(bulkSass())
        .pipe($.sass({ includePaths: [path.scss] }) )
        .pipe( gulp.dest('./assets/css/') );
});