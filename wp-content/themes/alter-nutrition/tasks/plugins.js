var $, browser_support, gulp, path ;

gulp = require("gulp");

$ = require("gulp-load-plugins")();

path = global.path;

browser_support = global.browser_support;

gulp.task('plugins', function() {
    return gulp.src(path.plugins + '*.js')
        .pipe($.concat('plugins.js'))
        .pipe(gulp.dest('assets/js/build/'))
        .pipe($.rename('plugins.min.js'))
        .pipe($.uglify())
        .pipe(gulp.dest('assets/js/build/'));
});
