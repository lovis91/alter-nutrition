var $, gulp, path, sprite_directory;

gulp = require("gulp");

$ = require("gulp-load-plugins")();

sprite_directory = "icons/";

path = global.path;

/*gulp.task('copyNonRetina',['sprite'], function(){
    gulp.src(path.sprite + "*-2x.png" )
        .pipe($.imageResize({
            width: '50%',
            height: '50%',
            imageMagick: true
        }))
        .pipe($.rename(function (image) { image.basename.replace('-2x', '');}))
        .pipe(gulp.dest(path.sprite))
});*/

gulp.task('sprite', function generateSpritesheets () {
    var spriteData = gulp.src(path.sprite + '*.png')
        .pipe($.spritesmith({
            // Filter out `-2x` (retina) images to separate spritesheet
            //   e.g. `github-2x.png`, `twitter-2x.png`
            retinaSrcFilter: '*-2x.png',

            // Generate a normal and a `-2x` (retina) spritesheet
            imgName: 'sprite.png',
            retinaImgName: 'sprite-2x.png',

            // Generate SCSS variables/mixins for both spritesheets
            cssName: '_sprite.scss',

            cssSpritesheetName: 'sprite',
            cssTemplate: path.scss + "tools/sprites/_sprite.mustache"
        }));

    // Deliver spritesheets to `dist/` folder as they are completed
    spriteData.img.pipe(gulp.dest(path.img));

    // Deliver CSS to `./` to be imported by `index.scss`
    spriteData.css.pipe(gulp.dest(path.scss + 'tools/sprites/'));
});