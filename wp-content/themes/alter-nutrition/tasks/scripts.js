var $, browserSync, browser_support, gulp, path, reload ;

gulp = require("gulp");

$ = require("gulp-load-plugins")();

path = global.path;

browser_support = global.browser_support;

gulp.task('scripts', function() {
    return gulp.src(path.js + '*.js')
        .pipe($.concat('scripts.js'))
        .pipe(gulp.dest('assets/js/build/'))
        .pipe($.rename('scripts.min.js'))
        .pipe($.uglify())
        .pipe(gulp.dest('assets/js/build/'));
});
