var $, gulp, path;

gulp = require("gulp");

$ = require("gulp-load-plugins")();

path = global.path;

gulp.task("default", function () {

    gulp.watch(path.scss + "**/*.scss", ["sass"]);
    gulp.watch(path.js + "*.js", ["scripts"]);
    gulp.watch(path.plugins + "*.js", ["plugins"]);
    return gulp.watch(path.img + "icons/*.png", ["sprite"]);

});