<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

?>


<footer class="site-footer" role="contentinfo">
    <div class="footer-social grid">
        <div class="col-1-1">
            <div class="social-icons">
                <?php
                query_posts(array('post_type' => 'social','order_by' => 'menu')); if(have_posts()) : while(have_posts()) : the_post();
                    get_template_part( 'content/content', 'footer-social' );
                endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>
    </div>
    <div class="pre-footer">
        <ul class="grid grid-pad">
            <?php
            query_posts(array('post_type' => 'footer', 'posts_per_page' => 5)); if(have_posts()) : while(have_posts()) : the_post();
                get_template_part( 'content/content', 'footer-parts' );
            endwhile; endif; wp_reset_query();
            ?>
        </ul>
    </div>
    <div class="footer-bottom">
        <div class="grid">
            <div class="col-1-1">
                <?php
                $widgetNL = new WYSIJA_NL_Widget(true);
                echo $widgetNL->widget(array('form' => 1, 'form_type' => 'php'));
                ?>
            </div>
        </div>
        <div class="grid">
            <div class="col-1-4">
                <nav class="site-navigation primary-navigation wrapper" role="navigation">
                    <?php wp_nav_menu( array( 'menu' => 'footer', 'menu_class' => 'nav-menu' ) ); ?>
                </nav>
            </div>
            <div class="col-1-4">
                <nav class="site-navigation primary-navigation wrapper" role="navigation">
                    <?php wp_nav_menu( array( 'menu' => 'footer-page', 'menu_class' => 'nav-menu' ) ); ?>
                </nav>
            </div>
            <div class="col-1-4 last-posts">
                <h3>Nos dernières actus</h3>
                <ul>
                <?php
                query_posts(array('post_type' => 'post','category_name' => 'actu', 'posts_per_page' => 3)); if(have_posts()) : while(have_posts()) : the_post();
                    get_template_part( 'content/content', 'footer-post' );
                endwhile; endif; wp_reset_query(); ?>
                </ul>
            </div>
            <div class="col-1-4 last-posts">
                <h3>Nos dernières recettes</h3>
                <ul>
                    <?php
                    query_posts(array('post_type' => 'post','category_name' => 'recette', 'posts_per_page' => 3)); if(have_posts()) : while(have_posts()) : the_post();
                        get_template_part( 'content/content', 'footer-post' );
                    endwhile; endif; wp_reset_query(); ?>
                </ul>
            </div>
        </div>
        <div class="grid">
            <div class="col-1-1">
                <div class="content">
                    <p class="copyright"> Copyright © 2016 <?= htmlspecialchars_decode(get_bloginfo('name')); ?> </p>
                </div>
            </div>
        </div>
    </div>

</footer><!-- #colophon -->
<?php wp_footer(); ?>
</body>
</html>