<?php
/**
 * The template for a slide in homeSlider
 */

$description = get_field('description');
$title = get_field('title');
$link = get_field('link');
$icon = get_field('icon');
?>

<li class="col-1-5">
    <div class="content slide slide-<?= htmlentities($title);?>">
        <a href="<?= $link ?>" title="<?= $title ?>">
            <i class="icon-<?=$icon?>"></i>
            <h3><?=$title?></h3>
            <p class="hidden">
                <?=$description?>
            </p>
        </a>
    </div>
</li>
