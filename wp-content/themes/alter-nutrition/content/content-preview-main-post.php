<?php
/**
 * The template for a main last post
 */
?>

<div class="col-1-1">
    <figure class="effect-bubba">
        <a href="<?= get_post_permalink(); ?>">
            <?php the_post_thumbnail('big', array('class' => 'alignleft')); ?>
            <figcaption>
                <h2><?php the_title(); ?></h2>
                <p><?php the_excerpt(); ?></p>
            </figcaption>
        </a>
    </figure>
</div>
