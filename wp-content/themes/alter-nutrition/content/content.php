<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$postId = get_post_thumbnail_id($post->ID);
$thumb = wp_get_attachment_image_src( $postId, 'post-thumbnail', false);
?>

<section id="article" class="article">
    <div class="container">

    </div>
</section>
