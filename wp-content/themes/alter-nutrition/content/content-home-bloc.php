<?php
/**
 * The template for a slide in homeSlider
 */

global $product;
$link = get_field('link');
$image = get_field('image');
$description = get_field('description');

?>

<div class="col-1-2">
    <figure class="effect-milo">
        <a href="<?=$link?>">
            <img src="<?= $image['url'] ?>" alt="<?= $image['alt'] ?>">
            <figcaption>
                <?= $description; ?>
            </figcaption>
        </a>
    </figure>
</div>