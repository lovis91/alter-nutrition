<?php
/**
 * The template for a last post
 */
?>

<li>
    <a href="<?= get_post_permalink(); ?>">
        <h4>
            <?php

            if (strlen($post->post_title) > 33)
                echo substr($post->post_title, 0, 30) . '...';
            else {
                echo $post->post_title;
            }
            ?>
        </h4>
        <div class="col-3-12">
            <?php the_post_thumbnail('thumbnail', array('class' => 'alignleft')); ?>

        </div>
        <div class="col-9-12">
            <p>
                <?php the_excerpt(); ?>
            </p>
        </div>
    </a>


</li>
