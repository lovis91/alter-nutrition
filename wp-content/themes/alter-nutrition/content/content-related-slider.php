<?php
/**
 * The template for a slide in homeSlider
 */

global $product;
?>

<li class="slide">
    <a class="global" href="<?=$product->get_permalink()?>" title="<?=$product->get_title();?>">
        <?= $product->get_image() ?>
    </a>
    <div class="flex-caption">
        <div class="content">
            <h3>
                <?=
                $product->get_title();
                ?>
            </h3>
            <p class="price">
                <span class="amount">
                    <?= $product->get_price()?> €
                </span>
            </p>
            <p class="decription">
            <?php
            echo mb_substr($product->post->post_content, 0, 100) . '...';
            ?>
            </p>
            <a class="read-more" href="<?=$product->get_permalink()?>" title="<?=$product->get_title();?>">
            En savoir plus
            </a>
        </div>
    </div>
</li>
