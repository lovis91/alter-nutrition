<?php
/**
 * The default template for socials
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
$link = get_field("link");
$icon = get_field("icon");
?>
<?php
if ($link !== "") {
    ?>
    <a target="_blank" href="<?= $link ?>">
        <i class="icon-<?= $icon ?>"></i>
    </a>
<?php
}
?>

