<?php
/**
 * The template for a slide in homeSlider
 */

$image = get_field('image');
$title = get_field('title');
$link = get_field('link');
?>

<li class="slide slide-<?= get_field('image')['id'];?> main-slide">
    <img src="<?= $image['url'] ?>" />
    <div class="container">
        <div class="flex-caption">
            <div class="content">
                <h2>
                    <?= $title;?>
                </h2>
            </div>
        </div>
    </div>

</li>