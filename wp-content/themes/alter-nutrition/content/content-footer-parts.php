<?php
/**
 * The template for a footer part
 */

$content = get_field('content');
$icon = get_field('icon');
$link = get_field('link');

?>
<li class="col-1-5 <?= $icon ?>">
    <a href="<?= $link ?>" title="<?= $content ?>">
        <i class="icon-<?= $icon ?>"></i>
        <p>
            <?= $content ?>
        </p>
    </a>
</li>