<?php
/**
 * The template for a slide in avantage
 */

$image = get_field('icon');
$title = get_field('title');
$desc = get_field('desc');
?>

<div class="col-1-2 avantage-col">
    <h3>
        <img src="<?= $image['url']; ?>" alt="<?= $title; ?>"/>
        <?= $title; ?>
    </h3>
    <p>
        <?= $desc; ?>
    </p>
</div>