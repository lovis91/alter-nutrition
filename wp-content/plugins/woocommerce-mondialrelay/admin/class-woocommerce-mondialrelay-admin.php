<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 * @author     Clément Barbaza <contact@mondialrelay-woocommerce.com>
 */
class WC_MondialRelay_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    /**
     * The current version of WooCommerce.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $current_version    The current version of this plugin.
     */
    private $current_version;

    /**
     * The position of the plugin menu.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $menu_position    The menu position of this plugin.
     */
    private $menu_position;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version           The version of this plugin.
     * @param      string    $current_version   The current version of this plugin.
     * @param      string    $menu_position     The menu position of this plugin.
	 */
	public function __construct($plugin_name, $version, $current_version, $menu_position) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->current_version = $current_version;
		$this->menu_position = $menu_position;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

        wp_enqueue_style( 'mondialrelay_admin', plugin_dir_url(dirname(__FILE__)).'admin/css/woocommerce-mondialrelay-admin.css', $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

        wp_register_script( 'jquery', plugin_dir_url('https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'));
        wp_enqueue_script('jquery', $this->version, false);
        wp_register_script( 'jquery_validate', plugin_dir_url(dirname(__FILE__)).'admin/js/jquery.validate.min.js', array( 'jquery'));
        wp_enqueue_script('jquery_validate', $this->version, false);
        wp_register_script( 'mondialrelay_admin_validate', plugin_dir_url(dirname(__FILE__)).'admin/js/woocommerce-mondialrelay-admin.js', array( 'jquery'));
        wp_enqueue_script('mondialrelay_admin_validate', $this->version, false);

	}

    /**
     * Show row meta on the plugin screen.
     *
     * @param	mixed $links Plugin Row Meta
     * @param	mixed $file  Plugin Base file
     * @return	array
     */
    public static function plugin_row_meta( $links, $file ) {
        if ( strpos( $file, WC_MondialRelay::PLUGIN_NAME ) !== false ) {
            $new_links = array(
                '<a href="' . WC_MondialRelay::DOCUMENTATION . '" target="_blank">' . __('Documentation', 'woocommerce_mondialrelay') . '</a>',
                '<a href="' . WC_MondialRelay::CLIENT . '" target="_blank">' . __('Espace client', 'woocommerce_mondialrelay') . '</a>',
            );
            $links = array_merge( $links, $new_links );
        }
        return $links;
    }

	/**
	 * Add a Mondial Relay menu
	 */
	public function MondialRelayMenu() {
		$mondialrelay_menu_position = get_option( 'mondialrelay_menu_position', WC_MondialRelay::MENU_POSITION );
		add_menu_page(
			__('Mondial Relay - Mondial Relay pour WooCommerce', 'woocommerce_mondialrelay'),
			__('Mondial Relay', 'woocommerce_mondialrelay'),
			'manage_woocommerce',
			'wc-mondialrelay',
			array($this, 'MondialRelayPage'),
			'dashicons-location',
			$mondialrelay_menu_position
		);
		add_submenu_page(
			'wc-mondialrelay',
			__('Expéditions - Mondial Relay pour WooCommerce', 'woocommerce_mondialrelay'),
			__('Expéditions', 'woocommerce_mondialrelay'),
			'manage_woocommerce',
			'wc-mondialrelay-expeditions',
            array($this, 'MondialRelayShipping')
		);
		add_submenu_page(
			'wc-mondialrelay',
			__('Maintenance - Mondial Relay pour WooCommerce', 'woocommerce_mondialrelay'),
			__('Maintenance', 'woocommerce_mondialrelay'),
			'manage_woocommerce',
			'wc-mondialrelay-maintenance',
            array($this, 'MondialRelayMaintenance')
		);

    }

	/**
	 * Notification when the plugin has an update available
	 */
	public function notificationMenu() {
		if (false !== ($contents = @file_get_contents($this->current_version))) {
			$current = file_get_contents($this->current_version);
		} else {
			$current = $this->version;
		}
		if (version_compare($this->version, $current, '<')) {
			$submenu['wc-mondialrelay'][5][0] .= "<span class='update-plugins count-1'><span class='update-count'>1</span></span>";
			$menu[$this->menu_position][0] .= "<span class='update-plugins count-1'><span class='update-count'>1</span></span>";
		}
	}

    /**
     * Mondial Relay options page
     * @param object $settings
     */
    function MondialRelayPage($settings) {
        global $wpdb;
        $prefixe = $wpdb->prefix;
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/woocommerce-mondialrelay-admin-page.php';
    }

    /**
     * Expeditions listing page of Mondial Relay
     * @param object $settings
     */
    function MondialRelayShipping($settings)
    {
        global $wpdb;
        $prefixe = $wpdb->prefix;
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/woocommerce-mondialrelay-admin-shipping.php';
    }

    /**
     * Mondial Relay in a WooCommerce order
     * @param  int $order_id
     */
    function actions_mr_meta_box($order_id) {
        add_meta_box(
            'id',
            __( 'Mondial Relay', 'actions_mondialrelay' ),
            array($this, 'actions_mr_meta_box_callback'),
            'shop_order',
            'side'
        );
    }

    /**
     * Mondial Relay function in a WooCommerce order
     * @param  object $order
     */
    function actions_mr_meta_box_callback( $order ) {
        global $wpdb;
        $prefixe = $wpdb->prefix;
        $order_id = $order->ID;

        // Check if the order uses Mondial Relay
        $is_mondial_relay = WC_MondialRelay_Sql::get_meta_champ($order_id, 'ID Point Mondial Relay', 1);
        if (isset($is_mondial_relay) && $is_mondial_relay == 1) {
            // Display the right box
            wp_nonce_field( 'actions_mr_meta_box', 'actions_mr_meta_box_nonce' );
            // Check if a Mondial Relay expedition already exists
            $exist_expedition = WC_MondialRelay_Sql::get_meta_champ($order_id, 'ExpeditionNum', 1);
            // A Mondial Relay expedition already exists for this order
            if (isset($exist_expedition) && $exist_expedition == 1) {
                $expedition_num = WC_MondialRelay_Sql::get_meta_champ($order_id, 'ExpeditionNum');
                echo '<p style="text-align:center">' . __('Expédition', 'woocommerce_mondialrelay')  . ' n°<strong>' . $expedition_num . '</strong></p>';
                // Get the global parameters of Mondial Relay account
                $params_compte = WC_MondialRelay_Order::get_params_compte();
                // Get the Mondial Relay point
                $id_mondial_relay = WC_MondialRelay_Sql::get_meta_champ($order_id, 'ID Point Mondial Relay');
                $explode_id_mondial_relay = explode('-', $id_mondial_relay);
                $liv_rel_pays_mondial_relay = $explode_id_mondial_relay[0];
                $liv_rel_mondial_relay = $explode_id_mondial_relay[1];
                // SOAP
                $client = new soapclient("http://www.mondialrelay.fr/WebService/Web_Services.asmx?WSDL");
                $params = array(
                    'Enseigne'       => $params_compte['mondialrelay_code_client'],
                    'Expeditions'    => $expedition_num,
                    'Langue'         => $liv_rel_pays_mondial_relay,
                );
                // Generation of the security key
                $code = implode("", $params);
                $code .= $params_compte['mondialrelay_cle_privee'];
                $params["Security"] = strtoupper(md5($code));
                $etiquette = $client->WSI2_GetEtiquettes($params)->WSI2_GetEtiquettesResult;
                if ($etiquette->STAT == 0) {
                    // Format 10x15
                    $etiquette->URL_PDF_10x15 = str_replace('A4', '10x15', $etiquette->URL_PDF_A4);
                    ?>
                    <hr><p style="text-align:center"><a class="button button-primary" target="_blank" href="http://www.mondialrelay.fr/<?php echo $etiquette->URL_PDF_A4 ?>"><?php _e('Étiquette A4', 'woocommerce_mondialrelay') ?></a></p> <p style="text-align:center"><a class="button button-primary" target="_blank" href="http://www.mondialrelay.fr/<?php echo $etiquette->URL_PDF_A5 ?>"><?php _e('Étiquette A5', 'woocommerce_mondialrelay') ?></a></p> <p style="text-align:center"><a class="button button-primary" target="_blank" href="http://www.mondialrelay.fr/<?php echo $etiquette->URL_PDF_10x15 ?>"><?php _e('Étiquette 10x15', 'woocommerce_mondialrelay') ?></a></p>
                    <?php
                }
                else {
                    echo '<p style="color: rgb(255,84,96);">' . __('Erreur', 'woocommerce_mondialrelay') . ' ' . $etiquette->STAT . '</p>';
                }

                // Colis tracking
                // En cours de création pour une future version :)
                /*
                $params = array(
                                   'Enseigne'       => $params_compte['mondialrelay_code_client'],
                                   'Expedition'    => $expedition_num,
                                   'Langue'         => $liv_rel_pays_mondial_relay,
                  );
                // Generation of the security key
                $code = implode("", $params);
                $code .= $params_compte['mondialrelay_cle_privee'];
                $params["Security"] = strtoupper(md5($code));
                $colis = $client->WSI2_TracingColisDetaille($params)->WSI2_TracingColisDetailleResult;
                echo __('Suivi colis', 'woocommerce_mondialrelay') . ' :<br>';
                varPrint($colis);
                */
            }
            // No Mondial Relay expedition for this order
            else {
                // Get the default parameters
                $mondialrelay_code_colis = get_option( 'mondialrelay_mode_colis', 1 );
                $mondialrelay_code_livraison = get_option( 'mondialrelay_mode_livraison', 1 );
                // Get the weight of the order
                $order = new WC_Order( $order_id );
                $items = $order->get_items();
                $poids_array = array();
                foreach ( $items as $item ) {
                    $product_id = $item['product_id'];
                    $poids = get_post_meta($product_id, '_weight');
                    if (isset($poids)) {
                        $poids = get_post_meta($product_id, '_weight',true);
                    } else {
                        $poids = "0";
                    }
                    $poids = $poids * $item['qty'];
                    // Convert the weight in g according to the WooCoomerce weight unit selected
                    $poids = WC_MondialRelay_Helpers::convert_poids($poids);
                    $poids_array[] = $poids;
                }
                $poids = array_sum($poids_array);
                echo '<div id="mr-notif" style="font-size: 13px; display:none"></div>';
                echo '<p><strong>' . __('Mode de collecte', 'woocommerce_mondialrelay') . '</strong></p><label class="screen-reader-text" for="ModeCol">' . __('Mode de collecte', 'woocommerce_mondialrelay') . '</label>'; ?>
                <select name="ModeCol" id="ModeCol"><option class="level-0" value="CCC" <?php if ($mondialrelay_code_colis == 'CCC') echo 'selected'; ?>>CCC</option><option class="level-0" value="CDR" <?php if ($mondialrelay_code_colis == 'CDR') echo 'selected'; ?>>CDR</option><option class="level-0" value="CDC" <?php if ($mondialrelay_code_colis == 'CDC') echo 'selected'; ?>>CDC</option><option class="level-0" value="REL" <?php if ($mondialrelay_code_colis == 'REL') echo 'selected'; ?>>REL</option></select>
                <?php echo '<p><strong>' . __('Mode de livraison', 'woocommerce_mondialrelay') . '</strong></p><label class="screen-reader-text" for="ModeLiv">' . __('Mode de livraison', 'woocommerce_mondialrelay') . '</label>'; ?>
                <select name="ModeLiv" id="ModeLiv"><option class="level-0" value="LCC" <?php if ($mondialrelay_code_livraison == 'LCC') echo 'selected'; ?>>LCC</option><option class="level-0" value="LD1" <?php if ($mondialrelay_code_livraison == 'LD1') echo 'selected'; ?>>LD1</option><option class="level-0" value="LDS" <?php if ($mondialrelay_code_livraison == 'LDS') echo 'selected'; ?>>LDS</option><option class="level-0" value="24R" <?php if ($mondialrelay_code_livraison == '24R') echo 'selected'; ?>>24R</option><option class="level-0" value="24L" <?php if ($mondialrelay_code_livraison == '24L') echo 'selected'; ?>>24L</option><option class="level-0" value="24X" <?php if ($mondialrelay_code_livraison == '24X') echo 'selected'; ?>>24X</option><option class="level-0" value="ESP" <?php if ($mondialrelay_code_livraison == 'ESP') echo 'selected'; ?>>ESP</option><option class="level-0" value="DRI" <?php if ($mondialrelay_code_livraison == 'DRI') echo 'selected'; ?>>DRI</option></select>
                <?php echo '<p><strong>' . __('Poids (en grammes)', 'woocommerce_mondialrelay') . '</strong></p><label class="screen-reader-text" for="Poids">' . __('Poids (en grammes)', 'woocommerce_mondialrelay') . '</label>'; ?>
                <input name="Poids" id="Poids" type="texte" value="<?php echo $poids ?>">
                <?php echo '<p><strong>' . __('Nombre de colis', 'woocommerce_mondialrelay') . '</strong></p><label class="screen-reader-text" for="NbColis">' . __('Nombre de colis', 'woocommerce_mondialrelay') . '</label><input type="texte" value="1" name="NbColis" id="NbColis">';
                echo '<p><strong>' . __('Contre-remboursement (en centimes)', 'woocommerce_mondialrelay') . '</strong></p><label class="screen-reader-text" for="CRT_Valeur">' . __('Contre-remboursement (en centimes)', 'woocommerce_mondialrelay') . '</label><input type="texte" value="0" name="CRT_Valeur" id="CRT_Valeur">';
                echo '<br><br><div style="text-align:center"><a href="#" id="create-expedition" class="button save_order button-primary" rel="'. $order_id . '">' . __('Créer une expédition', 'woocommerce_mondialrelay') . '</a></div>';
            }
        }
        else {
            echo "<em>" . __("Cette commande n'utilise pas Mondial Relay", "woocommerce_mondialrelay") . "</em>";
        }
    }

    /**
     * Javascript script for the creation of the Mondial Relay expedition
     */
    function expedition_javascript() { ?>
        <script type="text/javascript" >
            jQuery(document).ready(function($) {
                jQuery('#create-expedition').on('click', function() {
                    var th = jQuery(this);
                    var data = {
                        'action': 'expedition',
                        'order': th.attr('rel'),
                        'ModeCol': jQuery('#ModeCol').val(),
                        'ModeLiv': jQuery('#ModeLiv').val(),
                        'Poids': jQuery('#Poids').val(),
                        'NbColis': jQuery('#NbColis').val(),
                        'CRT_Valeur': jQuery('#CRT_Valeur').val()
                    };

                    $.post(ajaxurl, data, function(response) {
                        if (response != 0) {
                            var expedition = <?php echo __("'L\'expédition n\'a pas pu être créée'", 'woocommerce_mondialrelay') ?>;
                            var erreur = <?php echo __("'Erreur'", 'woocommerce_mondialrelay') ?>;
                            jQuery('#mr-notif').addClass('update-nag').css({ "margin": "0", "display": "block" }).html("<strong>"+expedition+"</strong><br>"+erreur+" "+response).show();
                        }
                        else if (response == 0) {
                            var expedition = <?php echo __("'L\'expédition a été créée'", 'woocommerce_mondialrelay') ?>;
                            jQuery('#mr-notif').addClass('updated').css({ "padding": "11px 15px" }).html("<strong>"+expedition+"</strong>").show();
                            setTimeout(function(){
                                window.location.reload();
                            }, 1000);
                        }
                        console.log(response);
                    });
                });
            });
        </script>
    <?php }

    /**
     * Expedition creation in a Mondial Relay order (AJAX)
     */
    function expedition_callback() {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        // Get the debug mode
        $mondialrelay_debug = get_option('mondialrelay_debug', WC_MondialRelay::DEBUG);

        // Get the order number
        $order_id = intval( $_POST['order'] );

        // Get the Mondial Relay parameters
        $params_compte = WC_MondialRelay_Order::get_params_compte();

        // Mondial Relay WebService
        $client = new soapclient("http://www.mondialrelay.fr/WebService/Web_Services.asmx?WSDL");

        // Get the Mondial Relay point
        $id_mondial_relay = WC_MondialRelay_Sql::get_meta_champ($order_id, 'ID Point Mondial Relay');
        $explode_id_mondial_relay = explode('-', $id_mondial_relay);
        $params_livraison = WC_MondialRelay_Order::get_params_livraison();
        $params_livraison['liv_rel_pays_mondial_relay'] = $explode_id_mondial_relay[0];
        $params_livraison['liv_rel_mondial_relay'] = $explode_id_mondial_relay[1];

        // Get the sender parameters
        $params_expediteur = WC_MondialRelay_Order::get_params_expediteur();

        // Get the recipient parameters
        $params_destinataire = WC_MondialRelay_Order::get_params_destinataire($order_id);

        // Get the order parameters
        $params_commande = WC_MondialRelay_Order::get_params_commande();

        // Get assurance
        $assurance = get_option('mondialrelay_assurance', WC_MondialRelay::ASSURANCE);

        // Get the collect point parameters
        $params_collecte = WC_MondialRelay_Order::get_params_collecte();
        // Mode REL
        if ($params_commande['ModeCol'] == 'REL') {
            $params_collecte['col_rel_pays_mondial_relay'] = $params_livraison['liv_rel_pays_mondial_relay'];
            $params_collecte['col_rel_mondial_relay'] = $params_livraison['liv_rel_mondial_relay'];
        }

        // Expedition parameters
        $params = array(
            'Enseigne'       => $params_compte['mondialrelay_code_client']
        ,'ModeCol'        => $params_commande['ModeCol']
        ,'ModeLiv'        => $params_commande['ModeLiv']
        ,'NDossier'       => $order_id
        ,'Expe_Langage'   => $params_expediteur['mondialrelay_vendeur_langage']
        ,'Expe_Ad1'       => $params_expediteur['mondialrelay_vendeur_adresse1']
        ,'Expe_Ad3'       => $params_expediteur['mondialrelay_vendeur_adresse3']
        ,'Expe_Ad4'       => $params_expediteur['mondialrelay_vendeur_adresse2']
        ,'Expe_Ville'     => $params_expediteur['mondialrelay_vendeur_ville']
        ,'Expe_CP'        => $params_expediteur['mondialrelay_vendeur_cp']
        ,'Expe_Pays'      => $params_expediteur['mondialrelay_vendeur_pays']
        ,'Expe_Tel1'      => $params_expediteur['mondialrelay_vendeur_tel']
        ,'Expe_Mail'      => $params_expediteur['mondialrelay_vendeur_email']
        ,'Dest_Langage'   => $params_destinataire['_shipping_langage']
        ,'Dest_Ad1'       => $params_destinataire['_billing_nom']
        ,'Dest_Ad3'       => $params_destinataire['_shipping_address_1']
        ,'Dest_Ville'     => $params_destinataire['_shipping_city']
        ,'Dest_CP'        => $params_destinataire['_shipping_postcode']
        ,'Dest_Pays'      => $params_destinataire['_shipping_country']
        ,'Dest_Tel1'      => $params_destinataire['_billing_phone']
        ,'Dest_Mail'      => $params_destinataire['_billing_email']
        ,'Poids'          => $params_commande['Poids']
        ,'NbColis'        => $params_commande['NbColis']
        ,'CRT_Valeur'     => $params_commande['CRT_Valeur']
        ,'LIV_Rel_Pays'   => $params_livraison['liv_rel_pays_mondial_relay']
        ,'LIV_Rel'        => $params_livraison['liv_rel_mondial_relay']
        ,'COL_Rel_Pays'	 => $params_collecte['col_rel_pays_mondial_relay']
        ,'COL_Rel'	 	 => $params_collecte['col_rel_mondial_relay']
        ,'Assurance'      => $assurance
        );

        // Generation of the security key
        $code = implode("", $params);
        $code .= $params_compte['mondialrelay_cle_privee'];
        $params["Security"] = strtoupper(md5($code));

        // Expedition creation
        $expedition = $client->WSI2_CreationExpedition($params)->WSI2_CreationExpeditionResult;
        $callback = $expedition->STAT;
        // Statut code
        if ($callback != 0) {
            echo $callback . '<br>' . WC_MondialRelay_Helpers::statut($callback);
            if ($mondialrelay_debug)
                WC_MondialRelay_Helpers::VarPrint($params);
        }
        elseif ($callback == 0) {
            // Update the checkout status in "Processing"
            $wpdb->update($prefixe . 'posts', array('post_status' => 'wc-processing'), array('ID' => $order_id));
            // Add the Mondial Relay expedition paramaters in the database
            $champs_bdd = WC_MondialRelay_Order::get_expedition_champs($expedition, $params_commande['NbColis']);
           WC_MondialRelay_Sql::insert_meta_champs($order_id, $champs_bdd);

            // Get the Mondial Relay email
            $email = WC_MondialRelay_Order::get_params_emails($order_id);
            $destinataire = $params_destinataire['_billing_email'];
            // Send the Mondial Relay tracking email to the client
            $email = wc_mail($destinataire, $email['sujet'], $email['message']);
        }

        die();
    }

    /**
     * Mondial Relay maintenance page
     * @param object $settings
     */
    function MondialRelayMaintenance($settings) {
        global $wpdb;
        $prefixe = $wpdb->prefix;
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/woocommerce-mondialrelay-admin-maintenance.php';

    }


}
