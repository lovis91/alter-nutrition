 <div class="wrap">
        <h2><?php _e("Expéditions Mondial Relay", 'woocommerce-mondialrelay') ?></h2>

        <?php
        // Page number (default : 1)
        if (isset($_GET['p']) && is_numeric($_GET['p']))
            $page = $_GET['p'];
        else
            $page = 1;
        $pagination = 20;
        $limit_start = ($page - 1) * $pagination;
        // Get the orders
        $myrows = WC_MondialRelay_Order::get_orders($limit_start, $pagination);
        ?>
        <table class="wp-list-table widefat fixed posts" width="100%" cellspacing="0">
            <thead>
            <th><?php _e("Etat", 'woocommerce-mondialrelay') ?></th>
            <th><?php _e("#", 'woocommerce-mondialrelay') ?></th>
            <th><?php _e("Client", 'woocommerce-mondialrelay') ?></th>
            <th><?php _e("Livraison", 'woocommerce-mondialrelay') ?></th>
            <th><?php _e("Date", 'woocommerce-mondialrelay') ?></th>
            <th><?php _e("Étiquettes", 'woocommerce-mondialrelay') ?></th>
            <th><?php _e("Expédition", 'woocommerce-mondialrelay') ?></th>
            </thead>
            <tbody>
            <?php
            // Mondial Relay orders counter
            $nb_cmd = 0;
            foreach ( $myrows as $myrow ) {
// Get the Mondial Relay order with expedition created
                $a_expedition = WC_MondialRelay_Sql::get_meta_champ($myrow->ID, 'ExpeditionNum');
                if ($a_expedition != '') {
                    $nb_cmd++;
// Get the status order
                    $etat = WC_MondialRelay_Helpers::etat_commande($myrow->post_status);
// Get the order metas
                    $champs = WC_MondialRelay_Configuration::get_order_meta();
                    $champs = WC_MondialRelay_Sql::get_meta_champs($myrow->ID, $champs);
// Get the labels in SOAP
                    // Get the globals Mondial Relay parameters
                    $params_compte = WC_MondialRelay_Order::get_params_compte();
                    // Get the Mondial Relay point
                    $id_mondial_relay = WC_MondialRelay_Sql::get_meta_champ($myrow->ID, 'ID Point Mondial Relay');
                    $explode_id_mondial_relay = explode('-', $id_mondial_relay);
                    $liv_rel_pays_mondial_relay = $explode_id_mondial_relay[0];
                    $liv_rel_mondial_relay = $explode_id_mondial_relay[1];
                    // Get the expedition
                    $expedition_num = WC_MondialRelay_Sql::get_meta_champ($myrow->ID, 'ExpeditionNum');
                    // SOAP
                    $client = new soapclient("http://www.mondialrelay.fr/WebService/Web_Services.asmx?WSDL");
                    $params = array(
                        'Enseigne'       => $params_compte['mondialrelay_code_client'],
                        'Expeditions'    => $expedition_num,
                        'Langue'         => $liv_rel_pays_mondial_relay,
                    );
                    // Generation of the security key
                    $code = implode("", $params);
                    $code .= $params_compte['mondialrelay_cle_privee'];
                    $params["Security"] = strtoupper(md5($code));
                    $etiquette = $client->WSI2_GetEtiquettes($params)->WSI2_GetEtiquettesResult;
                    // Format 10x15
                    $etiquette->URL_PDF_10x15 = str_replace('A4', '10x15', $etiquette->URL_PDF_A4);
                    ?>
                    <tr>
                        <td><?php echo $etat ?></td>
                        <td><a href="post.php?post=<?php echo $myrow->ID ?>&action=edit"><?php echo $myrow->ID ?></a></td>
                        <td><?php echo $champs['_billing_first_name'] . ' ' . $champs['_billing_last_name'] ?></td>
                        <td><?php echo $champs['_shipping_company'] . '<br>' . $champs['_shipping_address_1'] . '<br>' . $champs['_shipping_address_2'] . ' ' . $champs['_shipping_postcode'] . ' ' . $champs['_shipping_city'] . '<br>' . $champs['_shipping_country'] ?></td>
                        <td><?php echo $myrow->datefr ?></td>
                        <td><a class="button button-primary" target="_blank" href="http://www.mondialrelay.fr/<?php echo $etiquette->URL_PDF_A4 ?>"><?php _e("A4", 'woocommerce-mondialrelay') ?></a> <a class="button button-primary" target="_blank" href="http://www.mondialrelay.fr/<?php echo $etiquette->URL_PDF_A5 ?>"><?php _e("A5", 'woocommerce-mondialrelay') ?></a> <a class="button button-primary" target="_blank" href="http://www.mondialrelay.fr/<?php echo $etiquette->URL_PDF_10x15 ?>"><?php _e("10x15", 'woocommerce-mondialrelay') ?></a></td>
                        <td><?php echo $expedition_num ?></td>
                    </tr>
                <?php } } ?>
            </tbody>
        </table>
        <br>
        <div class="tablenav bottom">
            <div class="tablenav-pages"><span class="displaying-num"><?php echo $nb_cmd ?> <?php _e("éléments", 'woocommerce-mondialrelay') ?></span>
                <!-- PAGINATION -->
<span class="pagination-links">
<?php
$nb_pages = ceil($nb_cmd / $pagination);
if ($page == 1) {
    echo '<a class="first-page disabled" title="';
    _e("Etat", 'woocommerce-mondialrelay');
    echo '" href="#">‹</a>';
}
else {
    $precedent = $page - 1;
    echo '<a class="first-page disabled" title="';
    _e("Aller à la page pécédente", 'woocommerce-mondialrelay');
    echo '" href="?page=recrutement-page&p=' . $precedent . '">‹</a>';
}
for ($i = 1; $i <= $nb_pages; $i++) {
    if ($i == $page)
        echo '<a class="disabled" href="#">'. $i .'</a> ';
    else
        echo '<a href="?page=recrutement-page&p=' . $i . '">'. $i .'</a> ';
}
if ($page == $nb_pages) {
    echo '<a class="last-page disabled" title="';
    _e("Aller à la page pécédente", 'woocommerce-mondialrelay');
    echo '" href="#">›</a>';
}
else {
    $suivant = $page + 1;
    echo '<a class="last-page" title="';
    _e("Aller à la page pécédente", 'woocommerce-mondialrelay');
    echo '" href="?page=recrutement-page&p=' . $suivant . '">›</a>';
}
?>
</span></div>
            <br class="clear">
        </div>

    </div>