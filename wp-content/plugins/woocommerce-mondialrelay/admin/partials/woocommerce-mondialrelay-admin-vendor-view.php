<h2><?php _e("Coordonnées vendeur", 'woocommerce-mondialrelay') ?></h2>

<?php
if (isset($noty) && $noty == 1) { ?>
    <div class="updated">
        <p><?php _e("Les coordonnées du vendeur ont bien été mises à jour.", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php } ?>

<div id="message" class="updated woocommerce-message">
    <div class="squeezer">
        <p><strong><?php _e("Aide", 'woocommerce-mondialrelay') ?></strong></p>
        <p><?php _e("Pour plus d'informations sur les options du plugin, consultez", 'woocommerce-mondialrelay') ?> <a target="_blank" href="<?php echo WC_MondialRelay::DOCUMENTATION; ?>/vendeur"> <?php _e("la documentation", 'woocommerce-mondialrelay') ?></a>.</p>
    </div>
</div>

<h3><?php _e("Coordonnées du vendeur", 'woocommerce-mondialrelay') ?></h3>

<form method="post" action="" id="mondialrelay_admin_vendeur">

    <table class="form-table">
        <tbody>
        <?php
        foreach ($champs_vendeur as $key => $champ) { ?>
            <tr valign="top">
                <th scope="row"><label for="<?php echo $key ?>"><?php echo $labels_vendeur[$key]['label'] ?></label></th>
                <td>
                    <?php if (isset($labels_vendeur[$key]['type']) && $labels_vendeur[$key]['type'] == 'select') { ?>
                        <select name="<?php echo $key ?>">
                            <option value=""> <?php _e("Choisir", 'woocommerce-mondialrelay') ?>
                                <?php
                                foreach($labels_vendeur[$key]['options'] as $cle => $option) { ?>
                            <option value="<?php echo $option ?>" <?php if ($champs_vendeur[$key] == $option) echo 'selected'; ?>> <?php echo $cle ?>
                                <?php }
                                ?>
                        </select>
                    <?php } else { ?>
                        <input name="<?php echo $key ?>" type="text" id="<?php echo $key ?>" value="<?php echo $champ;?>" class="regular-text">
                    <?php } ?>
                    <p class="description"><?php if ($labels_vendeur[$key]['obligatoire'] == 1) _e('Obligatoire', 'woocommerce-mondialrelay'); else echo _e('Facultatif', 'woocommerce-mondialrelay'); ?></p>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e("Enregistrer", 'woocommerce-mondialrelay') ?>"></p>

</form>