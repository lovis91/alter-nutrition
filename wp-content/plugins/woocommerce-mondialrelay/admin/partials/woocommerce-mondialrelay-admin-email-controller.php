<?php

// Get the form
if (isset($_POST['submit']) && $_POST['email_expedition'] != '' && $_POST['sujet_expedition'] != '') {
    $champs['mondialrelay_email_expedition_message'] = str_replace(array('<br>','<br/>','<br />'),"\n\r",$_POST['email_expedition']);
    $champs['mondialrelay_email_expedition_sujet'] = $_POST['sujet_expedition'];
    $a_lien = strpos($champs['mondialrelay_email_expedition_message'], '[lien]');
    $a_expedition = strpos($champs['mondialrelay_email_expedition_message'], '[expedition]');
    if ($a_lien == true && $a_expedition == true) {
        // Update attributes
        WC_MondialRelay_Configuration::get_champs_email($champs);
        WC_MondialRelay_Sql::update_options_champs($champs);
        $noty = 2;
    }
    else {
        $noty = 1;
    }
}

// Attributes forms
$champs = array('mondialrelay_email_expedition_sujet' => '', 'mondialrelay_email_expedition_message' => '');
foreach($champs as $champ => $valeur) {
    if (isset($_POST[$champ]) && $_POST[$champ] != '')
        $champs[$champ] = $_POST[$champ];
    else {
        $champs[$champ] = $wpdb->get_var( "SELECT option_value FROM " . $prefixe . "options WHERE option_name = '$champ'" );
        $champs[$champ] = str_replace("<br />", "\n", $champs[$champ]);
    }
    $champs[$champ] = stripslashes($champs[$champ]);
}