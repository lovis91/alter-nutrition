<h2><?php _e("Emails", 'woocommerce-mondialrelay') ?></h2>
<?php
// Error message (shortcodes missing)
if (isset($noty) && $noty == 1) { ?>
    <div class="updated woocommerce-message wc-connect">
        <p><?php _e("Vous devez insérez obligatoirement les shortcodes de lien et d'expédition dans votre message", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php }
if (isset($noty) && $noty == 2) { ?>
    <div class="updated">
        <p><?php _e("L'email d'expédition a bien été modifié", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php } ?>

<div id="message" class="updated woocommerce-message">
    <div class="squeezer">
        <p><strong><?php _e("Aide", 'woocommerce-mondialrelay') ?></strong></p>
        <p><?php _e("Pour plus d'informations sur les options du plugin, consultez", 'woocommerce-mondialrelay') ?> <a target="_blank" href="<?php echo WC_MondialRelay::DOCUMENTATION; ?>/emails"> <?php _e("la documentation", 'woocommerce-mondialrelay') ?></a>.</p>
    </div>
</div>

<h3><?php _e("Email envoyé lors de la création d'une expédition", 'woocommerce-mondialrelay') ?></h3>
<p>
    <strong><?php _e("Shortcodes : ", 'woocommerce-mondialrelay') ?></strong><br>
    <?php _e("[lien] : affichage du lien de suivi Mondial Relay", 'woocommerce-mondialrelay') ?> <em><?php _e("(obligatoire)", 'woocommerce-mondialrelay') ?></em><br>
    <?php _e("[expedition] : numéro d'expédition de la commande Mondial Relay", 'woocommerce-mondialrelay') ?> <em><?php _e("(obligatoire)", 'woocommerce-mondialrelay') ?></em><br>
    <?php _e("[prenom] : Prénom du client", 'woocommerce-mondialrelay') ?><br>
    <?php _e("[nom] : Nom du client", 'woocommerce-mondialrelay') ?><br>
    <?php _e("[adresse] : Adresse de livraison Mondial Relay", 'woocommerce-mondialrelay') ?><br>
</p>
<p>
<form method="POST" id="mondialrelay_admin_emails">
    <table class="form-table">
        <tbody>
        <tr valign="top">
            <th scope="row"><label for="sujet_expedition"><?php _e("Sujet", 'woocommerce-mondialrelay') ?></label></th>
            <td><input name="sujet_expedition" type="text" id="sujet_expedition" value="<?php echo $champs['mondialrelay_email_expedition_sujet']; ?>" style="width: 100%;">
            </td>
        </tr>
        </tbody>
    </table>
    <textarea rows="15" style="width: 100%;" name="email_expedition"><?php echo $champs['mondialrelay_email_expedition_message']; ?></textarea>
    <input type="submit" name="submit" class="button save_order button-primary" value="<?php _e("Sauvegarder", 'woocommerce-mondialrelay') ?>">
</form>
</p>