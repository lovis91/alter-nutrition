<?php

// Attributes forms
if (isset($_POST['mondialrelay_id_livraison']) && $_POST['mondialrelay_id_livraison'] != '')
    $mondialrelay_id_livraison = $_POST['mondialrelay_id_livraison'];
else
    $mondialrelay_id_livraison = '';
if (isset($_POST['mondialrelay_pays_livraison']) && $_POST['mondialrelay_pays_livraison'] != '')
    $mondialrelay_pays_livraison = $_POST['mondialrelay_pays_livraison'];
else
    $mondialrelay_pays_livraison = 'FR';
if (isset($_POST['mondialrelay_code_postal']) && $_POST['mondialrelay_code_postal'] != '')
    $mondialrelay_code_postal = $_POST['mondialrelay_code_postal'];
else
    $mondialrelay_code_postal = '';
if (isset($_POST['mondialrelay_geolocalisation']) && $_POST['mondialrelay_geolocalisation'] != '')
    $mondialrelay_geolocalisation = $_POST['mondialrelay_geolocalisation'];
else
    $mondialrelay_geolocalisation = 'true';
if (isset($_POST['mondialrelay_pays_autorises']) && $_POST['mondialrelay_pays_autorises'] != '')
    $mondialrelay_pays_autorises_ = $_POST['mondialrelay_pays_autorises'];
else
    $mondialrelay_pays_autorises_ = '';
if (isset($_POST['mondialrelay_ssl']) && $_POST['mondialrelay_ssl'] != '')
    $mondialrelay_ssl = $_POST['mondialrelay_ssl'];
else
    $mondialrelay_ssl = 'false';
if (isset($_POST['mondialrelay_map']) && $_POST['mondialrelay_map'] != '')
    $mondialrelay_map = $_POST['mondialrelay_map'];
else
    $mondialrelay_map = 'true';
if (isset($_POST['mondialrelay_street_view']) && $_POST['mondialrelay_street_view'] != '')
    $mondialrelay_street_view = $_POST['mondialrelay_street_view'];
else
    $mondialrelay_street_view = 'false';
if (isset($_POST['mondialrelay_zoom']) && $_POST['mondialrelay_zoom'] != '')
    $mondialrelay_zoom = $_POST['mondialrelay_zoom'];
else
    $mondialrelay_zoom = 'false';
if (isset($_POST['mondialrelay_search_delay']) && $_POST['mondialrelay_search_delay'] != '')
    $mondialrelay_search_delay = $_POST['mondialrelay_search_delay'];
else
    $mondialrelay_search_delay = '';
if (isset($_POST['mondialrelay_search_far']) && $_POST['mondialrelay_search_far'] != '')
    $mondialrelay_search_far = $_POST['mondialrelay_search_far'];
else
    $mondialrelay_search_far = '75';
if (isset($_POST['mondialrelay_nb_results']) && $_POST['mondialrelay_nb_results'] != '')
    $mondialrelay_nb_results = $_POST['mondialrelay_nb_results'];
else
    $mondialrelay_nb_results = '7';
if (isset($_POST['mondialrelay_style']) && $_POST['mondialrelay_style'] != '')
    $mondialrelay_style = $_POST['mondialrelay_style'];
else
    $mondialrelay_style = '';
if (isset($_POST['mondialrelay_mode_recherche']) && $_POST['mondialrelay_mode_recherche'] != '')
    $mondialrelay_mode_recherche = $_POST['mondialrelay_mode_recherche'];
else
    $mondialrelay_mode_recherche = '24R';
if (isset($_POST['mondialrelay_texte_selection']) && $_POST['mondialrelay_texte_selection'] != '')
    $mondialrelay_texte_selection = $_POST['mondialrelay_texte_selection'];
else
    $mondialrelay_texte_selection = __("Cliquez ici pour choisir un point de livraison Mondial Relay", 'woocommerce-mondialrelay');
if (isset($_POST['mondialrelay_texte_aucun']) && $_POST['mondialrelay_texte_aucun'] != '')
    $mondialrelay_texte_aucun = $_POST['mondialrelay_texte_aucun'];
else
    $mondialrelay_texte_aucun = __("Vous n'avez pas sélectionné de point Mondial Relay", 'woocommerce-mondialrelay');
if (isset($_POST['mondialrelay_texte_choix']) && $_POST['mondialrelay_texte_choix'] != '')
    $mondialrelay_texte_choix = $_POST['mondialrelay_texte_choix'];
else
    $mondialrelay_texte_choix = __("Choisir ce point Mondial Relay", 'woocommerce-mondialrelay');
if (isset($_POST['mondialrelay_texte_erreur']) && $_POST['mondialrelay_texte_erreur'] != '')
    $mondialrelay_texte_erreur = $_POST['mondialrelay_texte_erreur'];
else
    $mondialrelay_texte_erreur = __("Veuillez sélectionner un point relais", 'woocommerce-mondialrelay');
if (isset($_POST['mondialrelay_parcelshop_picker']) && $_POST['mondialrelay_parcelshop_picker'] != '')
    $mondialrelay_parcelshop_picker = $_POST['mondialrelay_parcelshop_picker'];
else
    $mondialrelay_parcelshop_picker = '';

// Authorized contries
if (is_array($mondialrelay_pays_autorises_)) {
    $mondialrelay_pays_autorises = '';
    $i = 0;
    foreach ($mondialrelay_pays_autorises_ as $key => $value) {
        if ($i > 0)
            $mondialrelay_pays_autorises .= ',';
        $mondialrelay_pays_autorises .= $value;
        $i++;
    }
}
else
    $mondialrelay_pays_autorises = $mondialrelay_pays_autorises_;

// Save the attributes forms
$champs = array('mondialrelay_id_livraison' => $mondialrelay_id_livraison, 'mondialrelay_pays_livraison' => $mondialrelay_pays_livraison, 'mondialrelay_code_postal' => $mondialrelay_code_postal, 'mondialrelay_geolocalisation' => $mondialrelay_geolocalisation, 'mondialrelay_pays_autorises' => $mondialrelay_pays_autorises, 'mondialrelay_ssl' => $mondialrelay_ssl, 'mondialrelay_map' => $mondialrelay_map, 'mondialrelay_street_view' => $mondialrelay_street_view, 'mondialrelay_zoom' => $mondialrelay_zoom, 'mondialrelay_search_delay' => $mondialrelay_search_delay, 'mondialrelay_search_far' => $mondialrelay_search_far, 'mondialrelay_nb_results' => $mondialrelay_nb_results, 'mondialrelay_style' => $mondialrelay_style, 'mondialrelay_mode_recherche' => $mondialrelay_mode_recherche, 'mondialrelay_texte_selection' => $mondialrelay_texte_selection, 'mondialrelay_texte_aucun' => $mondialrelay_texte_aucun, 'mondialrelay_texte_choix' => $mondialrelay_texte_choix, 'mondialrelay_texte_erreur' => $mondialrelay_texte_erreur, 'mondialrelay_parcelshop_picker' => $mondialrelay_parcelshop_picker);

// Labels of the Mondial Relay pionts attributes form
$labels_points['mondialrelay_id_livraison']['label'] = __("Identifiants de livraison Mondial Relay", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_id_livraison']['description'] = __("Un ou plusieurs identifiants de méthodes de livraison liés à Mondial Relay (onglet livraison des paramètres de WooCommerce).<br><strong>Ex : local_pickup, gratuit", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_id_livraison']['type'] = "text";
$labels_points['mondialrelay_pays_livraison']['label'] = __("Pays de livraison Mondial Relay", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_pays_livraison']['description'] = __("Pays de livraison Mondial Relay.<br><strong>Ex : France</strong> (par défaut)", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_pays_livraison']['options'] = WC_MondialRelay_Helpers::get_pays();
$labels_points['mondialrelay_pays_livraison']['type'] = "select";
$labels_points['mondialrelay_pays_autorises']['label'] = __("Pays autorisés", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_pays_autorises']['description'] = __("Liste des pays que peut sélectionner l'utilisateur pour la recherche<br><strong>Ex : France, Espagne", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_pays_autorises']['options'] = WC_MondialRelay_Helpers::get_pays();
$labels_points['mondialrelay_pays_autorises']['type'] = "multiple";
$labels_points['mondialrelay_code_postal']['label'] = __("Code Postal Mondial Relay", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_code_postal']['description'] = __("Code postal de livraison Mondial Relay par défaut (optionnel).<br><strong>Ex : 31000</strong>", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_code_postal']['type'] = "text";
$labels_points['mondialrelay_geolocalisation']['label'] = __("Géolocalisation", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_geolocalisation']['description'] = __("Activer ou non la possiblilité à l'utilisateur de géolocaliser les points Mondial Relay autour de lui (oui par défaut).", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_geolocalisation']['type'] = "radio";
$labels_points['mondialrelay_map']['label'] = __("Affichage de la carte", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_map']['description'] = __("Afficher ou non les résultats sur une carte (oui par défaut).", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_map']['type'] = "radio";
$labels_points['mondialrelay_zoom']['label'] = __("Zoom on scroll", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_zoom']['description'] = __("Activer ou non le zoom on scroll sur la carte des résultats (non par défaut).", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_zoom']['type'] = "radio";
$labels_points['mondialrelay_street_view']['label'] = __("Street View", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_street_view']['description'] = __("Activer ou non le mode Street View sur la carte des résultats (non par défaut).", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_street_view']['type'] = "radio";
$labels_points['mondialrelay_ssl']['label'] = __("SSL", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_ssl']['description'] = __("Activer ou non la communication avec les serveurs Mondial Relay en HTTPS (non par défaut).", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_ssl']['type'] = "radio";
$labels_livraison['mondialrelay_mode_recherche']['label'] = __("Mode de recherche", 'woocommerce-mondialrelay');
$labels_livraison['mondialrelay_mode_recherche']['description'] = __("Le mode de recherche de collecte ou de livraison.<br><strong>Ex : 24R </strong> (par défaut)", 'woocommerce-mondialrelay');
$labels_livraison['mondialrelay_mode_recherche']['type'] = "select";
$labels_livraison['mondialrelay_mode_recherche']['options'] = array('REL : Collecte en point relais' => 'REL', '24R : recherche en Point Relais Standard' => '24R', '24L : recherche en Point Relais XL' => '24L', '24X : recherche en Point Relais XXL' => '24X', 'DRI : recherche en Colis Drive' => 'DRI');
$labels_points['mondialrelay_search_delay']['label'] = __("Nombre de jour entre la recherche et le dépôt du colis", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_search_delay']['description'] = __("Le nombre de jour entre la recherche et le dépôt du colis (vide par défaut).<br>Cette option permet de filtrer les points relais qui seraient éventuellement en congés au moment de la livraison.", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_search_delay']['type'] = "text";
$labels_points['mondialrelay_search_far']['label'] = __("Distance maximale de recherche de points relais", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_search_far']['description'] = __("Limiter la recherche de points relais à une distance maximale.<br> <strong>Ex : 75</strong> (par défaut).", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_search_far']['type'] = "text";
$labels_points['mondialrelay_nb_results']['label'] = __("Nombre de points relais à renvoyer", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_nb_results']['description'] = __("Le nombre de points relais à afficher.<br><strong>Ex : 7</strong> (par défaut).", 'woocommerce-mondialrelay');
$labels_points['mondialrelay_nb_results']['type'] = "text";

// Labels of the webmarketing Mondial Relay attributes form
$labels_webmarketing['mondialrelay_texte_selection']['label'] = __("Texte de sélection de point relais", 'woocommerce-mondialrelay');
$labels_webmarketing['mondialrelay_texte_selection']['description'] = '<strong>' . __("Ex : Cliquez ici pour choisir un point de livraison Mondial Relay", 'woocommerce-mondialrelay') . '</strong> ' . __("(par défaut)", 'woocommerce-mondialrelay');
$labels_webmarketing['mondialrelay_texte_selection']['type'] = "textarea";
$labels_webmarketing['mondialrelay_texte_aucun']['label'] = __("Texte si aucun point relais n'est sélectionné", 'woocommerce-mondialrelay');
$labels_webmarketing['mondialrelay_texte_aucun']['description'] = '<strong>' . __("Ex : Vous n'avez pas sélectionné de point Mondial Relay", 'woocommerce-mondialrelay') . '</strong> ' . __("(par défaut)", 'woocommerce-mondialrelay');
$labels_webmarketing['mondialrelay_texte_aucun']['type'] = "textarea";
$labels_webmarketing['mondialrelay_texte_choix']['label'] = __("Texte du bouton de sélection de point relais", 'woocommerce-mondialrelay');
$labels_webmarketing['mondialrelay_texte_choix']['description'] = '<strong>' . __("Ex : Choisir ce point Mondial Relay", 'woocommerce-mondialrelay') . '</strong> ' . __("(par défaut)", 'woocommerce-mondialrelay');
$labels_webmarketing['mondialrelay_texte_choix']['type'] = "textarea";
$labels_webmarketing['mondialrelay_texte_erreur']['label'] = __("Texte d'erreur si l'utilisateur n'a pas sélectionné de point relais", 'woocommerce-mondialrelay');
$labels_webmarketing['mondialrelay_texte_erreur']['description'] = '<strong>' . __("Ex : Veuillez sélectionner un point relais", 'woocommerce-mondialrelay') . '</strong> ' . __("(par défaut)", 'woocommerce-mondialrelay');
$labels_webmarketing['mondialrelay_texte_erreur']['type'] = "textarea";

// Labels of the specific Mondial Relay attributes form
$labels_specifiques['mondialrelay_style']['label'] = __("Style CSS spécifique", 'woocommerce-mondialrelay');
$labels_specifiques['mondialrelay_style']['description'] = __("Surcharger le style CSS de votre thème sur la page \"Checkout\"", 'woocommerce-mondialrelay');
$labels_specifiques['mondialrelay_style']['type'] = "textarea";

// Labels of the Mondial Relay parcelshop picker attributes form
$labels_widget['mondialrelay_parcelshop_picker']['label'] = __("Version de Parcelshop Picker", 'woocommerce-mondialrelay');
$labels_widget['mondialrelay_parcelshop_picker']['description'] = __("Version du widget Parcelshop Picker de Mondial Relay<br><strong>Ex : 3.2</strong> (par défaut)", 'woocommerce-mondialrelay');
$labels_widget['mondialrelay_parcelshop_picker']['type'] = "select";
$labels_widget['mondialrelay_parcelshop_picker']['options'] = array('3.2', '3.1', '3.0');

// Update options
if (isset($_POST['submit'])) {
    $envoi = 1;
    $noty = 1;
    WC_MondialRelay_Sql::update_options_champs($champs);
}

// Get the options
$champs = WC_MondialRelay_Sql::get_options_champs($champs);
$old_pays_autorises = explode(',', $champs['mondialrelay_pays_autorises']);