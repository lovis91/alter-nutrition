<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link              http://www.mondialrelay-woocommerce.com
 * @since             1.0.0
 * @package           WC_MondialRelay
 */

// Attributes forms
if (isset($_POST['mondialrelay_code_client']) && $_POST['mondialrelay_code_client'] != '')
    $mondialrelay_code_client = $_POST['mondialrelay_code_client'];
else
    $mondialrelay_code_client = 'BDTEST13';
if (isset($_POST['mondialrelay_cle_privee']) && $_POST['mondialrelay_cle_privee'] != '')
    $mondialrelay_cle_privee = $_POST['mondialrelay_cle_privee'];
else
    $mondialrelay_cle_privee = 'PrivateK';
if (isset($_POST['mondialrelay_mode_colis']) && $_POST['mondialrelay_mode_colis'] != '')
    $mondialrelay_mode_colis = $_POST['mondialrelay_mode_colis'];
else
    $mondialrelay_mode_colis = 'CCC';
if (isset($_POST['mondialrelay_mode_livraison']) && $_POST['mondialrelay_mode_livraison'] != '')
    $mondialrelay_mode_livraison = $_POST['mondialrelay_mode_livraison'];
else
    $mondialrelay_mode_livraison = '24R';
if (isset($_POST['mondialrelay_assurance']) && $_POST['mondialrelay_assurance'] != '')
    $mondialrelay_assurance = $_POST['mondialrelay_assurance'];
else
    $mondialrelay_assurance = '0';

// Save the attributes forms
$champs = array('mondialrelay_code_client' => $mondialrelay_code_client, 'mondialrelay_cle_privee' => $mondialrelay_cle_privee, 'mondialrelay_mode_colis' => $mondialrelay_mode_colis, 'mondialrelay_mode_livraison' => $mondialrelay_mode_livraison, 'mondialrelay_assurance' => $mondialrelay_assurance);

// Labels of the general Mondial Relay attributes form
$labels_compte['mondialrelay_code_client']['label'] = __("Code Client Mondial Relay", 'woocommerce-mondialrelay');
$labels_compte['mondialrelay_code_client']['description'] = __("Votre code client Mondial Relay. Sans ce code, le plugin Mondial Relay fonctionnera en mode démo.<br><strong>Ex : BDTEST13 </strong> (mode démo)", 'woocommerce-mondialrelay');
$labels_compte['mondialrelay_code_client']['type'] = "text";
$labels_compte['mondialrelay_cle_privee']['label'] = __("Clé privée Mondial Relay", 'woocommerce-mondialrelay');
$labels_compte['mondialrelay_cle_privee']['description'] = __("Votre clé privée Mondial Relay. Obligatoire pour utiliser les fonctions d'expéditions et d'étiquettages du plugin.<br><strong>Ex : PrivateK </strong> (mode démo)", 'woocommerce-mondialrelay');
$labels_compte['mondialrelay_cle_privee']['type'] = "text";

// Labels of the shipping Mondial Relay attributes form
$labels_livraison['mondialrelay_mode_colis']['label'] = __("Mode de collecte", 'woocommerce-mondialrelay');
$labels_livraison['mondialrelay_mode_colis']['description'] = __("Le mode de collecte de colis par défaut.<br><strong>Ex : CCC </strong> (par défaut)", 'woocommerce-mondialrelay');
$labels_livraison['mondialrelay_mode_colis']['type'] = "select";
$labels_livraison['mondialrelay_mode_colis']['options'] = array('CCC : Collecte chez le client chargeur / l\'enseigne' => 'CCC', 'CDR : Collecte à domicile pour les expéditions standards' => 'CDR', 'CDS : Collecte à domicile pour les expéditions lourdes ou volumineuses' => 'CDS', 'REL : Collecte en point relais' => 'REL');
$labels_livraison['mondialrelay_mode_livraison']['label'] = __("Mode de livraison", 'woocommerce-mondialrelay');
$labels_livraison['mondialrelay_mode_livraison']['description'] = __("Le mode de livraison de colis par défaut.<br><strong>Ex : 24R </strong> (par défaut)", 'woocommerce-mondialrelay');
$labels_livraison['mondialrelay_mode_livraison']['type'] = "select";
$labels_livraison['mondialrelay_mode_livraison']['options'] = array('LCC : Livraison chez le client chargeur / l\'enseigne' => 'LCC', 'LD1 : Livraison à domicile pour les expéditions standards' => 'LD1', 'LDS : Livraison à domicile pour les expéditions lourdes ou volumineuses' => 'LDS', '24R : Livraison en Point Relais Standard' => '24R', '24L : Livraison en Point Relais XL' => '24L', '24X : Livraison en Point Relais XXL' => '24X', 'DRI : Livraison en Colis Drive' => 'DRI');
$labels_livraison['mondialrelay_assurance']['label'] = __("Assurance", 'woocommerce-mondialrelay');
$labels_livraison['mondialrelay_assurance']['description'] = __("Le niveau d'assurance de votre contrat Mondial Relay.<br><strong>Ex: 0</strong> (par défaut)", 'woocommerce-mondialrelay');
$labels_livraison['mondialrelay_assurance']['type'] = "select";
$labels_livraison['mondialrelay_assurance']['options'] = array('0 : Sans assurance' => 0, '1 : Assurance complémentaire N1' => 1, '2 : Assurance complémentaire N2' => 2, '3 : Assurance complémentaire N3' => 3, '4 : Assurance complémentaire N4' => 4, '5 : Assurance complémentaire N5' => 5);

// Update options
if (isset($_POST['submit'])) {
    $envoi = 1;
    $noty = 1;
    WC_MondialRelay_Sql::update_options_champs($champs);
}

// Get the options
$champs = WC_MondialRelay_Sql::get_options_champs($champs);
?>