<h2><?php _e("Maintenance du plugin Mondial Relay", 'woocommerce-mondialrelay') ?></h2>
<?php
// Reset all Mondial Relay parameters message
if (isset($noty) && $noty == 1) { ?>
    <div class="updated">
        <p><?php _e("Tous les paramètres du plugin Mondial Relay pour WooCommerce ont bien été réinitialisés.", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php }
// Reset configuration Mondial Relay parameters message
else if (isset($noty) && $noty == 2) { ?>
    <div class="updated">
        <p><?php _e("Les paramètres de configuration du plugin Mondial Relay pour WooCommerce ont bien été réinitialisés.", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php }
// Reset point Mondial Relay parameters message
else if (isset($noty) && $noty == 3) { ?>
    <div class="updated">
        <p><?php _e("Les paramètres de recherche de point relais du plugin Mondial Relay pour WooCommerce ont bien été réinitialisés.", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php }
// Reset vendor contact details Mondial Relay parameters message
else if (isset($noty) && $noty == 4) { ?>
    <div class="updated">
        <p><?php _e("Les coordonnées du vendeur sur le plugin Mondial Relay pour WooCommerce ont bien été réinitialisées.", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php }
// Reset email Mondial Relay parameters message
else if (isset($noty) && $noty == 5) { ?>
    <div class="updated">
        <p><?php _e("Les paramètres d'emails de plugin Mondial Relay pour WooCommerce ont bien été réinitialisés.", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php }
// Debug
if (isset($noty) && $noty == 7) { ?>
    <div class="updated">
        <p><?php _e("Le mode debug a bien été modifié", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php }
// Menu position
if (isset($noty) && $noty == 8) { ?>
    <div class="updated">
        <p><?php _e("La position du plugin a bien été modifiée dans le menu de Wordpress<br>Rechargez la page pour voir apparaître la nouvelle position.", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php } ?>

<div id="message" class="updated woocommerce-message">
    <div class="squeezer">
        <p><strong><?php _e("Aide", 'woocommerce-mondialrelay') ?></strong></p>
        <p><?php _e("Pour plus d'informations sur les options du plugin, consultez", 'woocommerce-mondialrelay') ?> <a target="_blank" href="<?php echo WC_MondialRelay::DOCUMENTATION; ?>/maintenance"> <?php _e("la documentation", 'woocommerce-mondialrelay') ?></a>.</p>
    </div>
</div>

<h3><?php _e("Mise à jour du plugin Mondial Relay pour WooCommerce", 'woocommerce-mondialrelay') ?></h3>
<?php if (isset($maj) && $maj == 1) { ?>
    <div class="update-nag" style="margin:0; display:block; padding: 1px 12px;">
        <p>
            <?php echo sprintf( __("Votre version %s du plugin Mondial Relay pour WooCommerce n'est pas à jour.", 'woocommerce-mondialrelay'), $mondialrelay_version); ?>
            <br>
            <?php echo sprintf( __("La version %s est disponible en téléchargement.", 'woocommerce-mondialrelay'), $current); ?>
            <br><br><a href='http://client.mondialrelay-woocommerce.com' target='_blank' class='button-primary'><?php  _e("Télécharger la dernière version du plugin Mondial Relay pour WooCommerce", 'woocommerce-mondialrelay') ?></a>
        </p>
    </div>
<?php } else { ?>
<div class="updated">
    <p><?php _e("Votre version du plugin Mondial Relay pour WooCommerce est à jour.", 'woocommerce-mondialrelay') ?></p>
    <?php } ?>
</div>
<br><hr>
<h3><?php _e("Test du webservice Mondial Relay", 'woocommerce-mondialrelay') ?></h3>
<?php
if (isset($noty) && $noty == 6) {
    if ($callback == 0) {
        echo '<p><strong>';
        _e("Configuration du compte Mondial Relay", 'woocommerce-mondialrelay');
        echo '</strong></p><div id="message" class="updated"><div class="squeezer"><p><strong>';
        _e("Succès", 'woocommerce-mondialrelay');
        echo '</strong><br>';
        _e("Le WebService Mondial Relay est correctement configuré", 'woocommerce-mondialrelay');
        echo '.</p></div></div>';
    }
    else {
        echo '<p><strong>';
        _e("Configuration du compte Mondial Relay", 'woocommerce-mondialrelay');
        echo '</strong></p><div id="message" class="update-nag" style="margin:0; display:block; padding: 1px 12px;"><div class="squeezer"><p><strong>';
        _e("Erreur", 'woocommerce-mondialrelay');
        echo ' ' . $callback . '</strong><br>' . $libelle_retour . '</p></div></div>';
    }
    if ($mondialrelay_debug == 1)
        WC_MondialRelay_Helpers::varPrint($params_vendeur);
    if ($vendeur == 0) {
        echo '<p><strong>';
        _e("Configuration du vendeur Mondial Relay", 'woocommerce-mondialrelay');
        echo '</strong></p><div id="message" class="updated"><div class="squeezer"><p><strong>';
        _e("Succès", 'woocommerce-mondialrelay');
        echo '</strong><br>';
        _e("Les coordonnées du vendeur sont correctement remplies.", 'woocommerce-mondialrelay');
        echo '</p></div></div>';
    }
    else {
        echo '<p><strong>';
        _e("Configuration du vendeur Mondial Relay", 'woocommerce-mondialrelay');
        echo '</strong></p><div id="message" class="update-nag" style="margin:0; display:block; padding: 1px 12px;"><div class="squeezer"><p><strong>';
        _e("Erreur", 'woocommerce-mondialrelay');
        echo ' ' . $vendeur . '</strong><br>' . $libelle_vendeur_retour . '</p></div></div>';
    }
    if ($mondialrelay_debug == 1)
        WC_MondialRelay_Helpers::varPrint($params_vendeur);
}
?>
<p><?php _e("Pour tester le bon fonctionnement du webservice Mondial Relay avec votre configuration, cliquez sur le bouton ci-dessous :", 'woocommerce-mondialrelay') ?></p>
<form method="POST">
    <input type="submit" name="webservice" class="button save_order button-primary" value="<?php _e("Tester le webservice", 'woocommerce-mondialrelay') ?>">
</form>
<br><hr>
<h3><?php _e("Mode Debug", 'woocommerce-mondialrelay') ?></h3>
<form method="post" action="" id="mondialrelay_admin_debug">
    <table class="form-table">
        <tbody>
        <tr valign="top">
            <th scope="row"><label for="mondialrelay_debug"><?php _e('Activer le mode debug ?', 'woocommerce-mondialrelay') ?> </label></th>
            <td>
                <input type="radio" name="mondialrelay_debug" id="mondialrelay_debug_oui" value="1" <?php if ($mondialrelay_debug == true) echo 'checked'; ?>><label for="mondialrelay_debug_oui">Oui</label>
                <input type="radio" name="mondialrelay_debug" id="mondialrelay_debug_non" value="0" <?php if ($mondialrelay_debug == false) echo 'checked'; ?>><label for="mondialrelay_debug_non">Non</label>
                <p class="description"><?php _e("Permet d'afficher les logs concernant les fonctionnalités du plugin Mondial Relay pour WooCommerce<br>\"Non\" par défaut", 'woocommerce-mondialrelay') ?></p>
            </td>
        </tr>
        </tbody>
    </table>
    <p class="submit"><input type="submit" name="debug" id="debug" class="button button-primary" value="<?php _e("Enregistrer", 'woocommerce-mondialrelay') ?>"></p>
</form>
<hr>
<h3><?php _e("Position du plugin dans le menu latéral", 'woocommerce-mondialrelay') ?></h3>
<form method="post" action="" id="mondialrelay_admin_menu_position">
    <table class="form-table">
        <tbody>
        <tr valign="top">
            <th scope="row"><label for="mondialrelay_debug"><?php _e('Position', 'woocommerce-mondialrelay') ?> </label></th>
            <td>
                <input type="text" name="mondialrelay_menu_position" id="mondialrelay_menu_position" value="<?php echo $mondialrelay_menu_position; ?>">
                <p class="description"><?php _e("Position du menu dans le menu latéral de Wordpress<br>58.1 par défaut", 'woocommerce-mondialrelay') ?></p>
            </td>
        </tr>
        </tbody>
    </table>
    <p class="submit"><input type="submit" name="mondialrelay_position" id="mondialrelay_position" class="button button-primary" value="<?php _e("Enregistrer", 'woocommerce-mondialrelay') ?>"></p>
</form>
<hr>
<h3><?php _e("Réinitialisation des paramètres", 'woocommerce-mondialrelay') ?></h3>
<table class="wp-list-table widefat fixed striped">
    <tr>
        <td>
            <?php _e("Réinitialiser tous vos paramètres relatifs au plugin Mondial Relay pour WooCommerce, et \"nettoyer\" votre base de données Wordpress de toutes les entrées spécifiques à ce plugin", 'woocommerce-mondialrelay') ?>
        </td>
        <td style="text-align:right; vertical-align: middle;">
            <a href="?page=wc-mondialrelay-maintenance&all=1" onclick="return(confirm('<?php _e("Etes-vous sûr de vouloir réinitialiser TOUS vos paramètres Mondial Relay ?", 'woocommerce-mondialrelay') ?>'));"  class="button button-primary"><?php _e("Réinitialiser tous les paramètres Mondial Relay", 'woocommerce-mondialrelay') ?></a>
        </td>
    </tr>
    <tr>
        <td>
            <?php _e("Réinitialiser uniquement les paramètres de configuration du plugin Mondial Relay pour WooCommerce", 'woocommerce-mondialrelay') ?>
        </td>
        <td  style="text-align:right; vertical-align: middle;">
            <a href="?page=wc-mondialrelay-maintenance&conf=1" onclick="return(confirm('<?php _e("Etes-vous sûr de vouloir réinitialiser les paramètres de configuration Mondial Relay ?", 'woocommerce-mondialrelay') ?>'));" class="button button-primary"><?php _e("Réinitialiser les paramètres de compte Mondial Relay", 'woocommerce-mondialrelay') ?></a>
        </td>
    </tr>
    <tr>
        <td>
            <?php _e("Réinitialiser uniquement les paramètres de recherche de points relais du plugin Mondial Relay pour WooCommerce", 'woocommerce-mondialrelay') ?>
        </td>
        <td style="text-align:right; vertical-align: middle;">
             <a href="?page=wc-mondialrelay-maintenance&points=1" onclick="return(confirm('<?php _e("Etes-vous sûr de vouloir réinitialiser les paramètres de configuration Mondial Relay ?", 'woocommerce-mondialrelay') ?>'));" class="button button-primary"><?php _e("Réinitialiser les paramètres de recherche de point relais Mondial Relay", 'woocommerce-mondialrelay') ?></a>
        </td>
    </tr>
    <tr>
        <td>
            <?php _e("Réinitialiser vos coordonnées vendeur du plugin Mondial Relay pour WooCommerce", 'woocommerce-mondialrelay') ?>
        </td>
        <td style="text-align:right; vertical-align: middle;">
            <a href="?page=wc-mondialrelay-maintenance&vendeur=1" onclick="return(confirm('<?php _e("Etes-vous sûr de vouloir réinitialiser vos paramètres vendeur ?", 'woocommerce-mondialrelay') ?>'));"  class="button button-primary"><?php _e("Réinitialiser les paramètres vendeur", 'woocommerce-mondialrelay') ?></a>
        </td>
    </tr>
    <tr>
        <td>
            <?php _e("Réinitialiser vos paramètres d'emails du plugin Mondial Relay pour WooCommerce", 'woocommerce-mondialrelay') ?>
        </td>
        <td style="text-align:right; vertical-align: middle;">
            <a href="?page=wc-mondialrelay-maintenance&emails=1" onclick="return(confirm('<?php _e("Etes-vous sûr de vouloir réinitialiser vos paramètres emails ?", 'woocommerce-mondialrelay') ?>'));"  class="button button-primary"><?php _e("Réinitialiser les paramètres emails", 'woocommerce-mondialrelay') ?></a>
        </td>
    </tr>
</table>