<?php
// MAINTENANCE
if (!isset($_GET['tab']) || $_GET['tab'] == 'maintenance')
require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-maintenance-controller.php';
// STATUS
elseif ($_GET['tab'] == 'status')
require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-status-controller.php';
?>
<div class="wrap woocommerce">
    <h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
        <a href="?page=wc-mondialrelay-maintenance&amp;tab=maintenance" class="nav-tab <?php if (!isset($_GET['tab']) || $_GET['tab'] == 'maintenance') echo 'nav-tab-active'; ?>"><?php _e("Maintenance", 'woocommerce-mondialrelay') ?></a><a href="?page=wc-mondialrelay-maintenance&amp;tab=status" class="nav-tab <?php if ($_GET['tab'] == 'status') echo 'nav-tab-active'; ?>"><?php _e("Statut", 'woocommerce-mondialrelay') ?></a>	</h2>
    <br>

    <?php
    // MAINTENANCE
    if (!isset($_GET['tab']) || $_GET['tab'] == 'maintenance')
        require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-maintenance-view.php';
    // STATUS
    elseif ($_GET['tab'] == 'status')
        require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-status-view.php';
    ?>
</div>