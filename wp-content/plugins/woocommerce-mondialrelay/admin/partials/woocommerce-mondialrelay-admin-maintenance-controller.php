<?php

// Reset all Mondial Relay parameters
if (isset($_GET['all'])) {
    $champs = WC_MondialRelay_Configuration::get_champs_mondialrelay();
    $noty = 1;
}
// Reset Mondial Relay confguration parameters
elseif (isset($_GET['conf'])) {
    $champs = WC_MondialRelay_Configuration::get_champs_configuration();
    $noty = 2;
}
// Reset Mondial Relay point parameters
elseif (isset($_GET['points'])) {
    $champs = WC_MondialRelay_Configuration::get_champs_points();
    $noty = 3;
}
// Reset Mondial Relay vendor parameters
elseif (isset($_GET['vendeur'])) {
    $champs = WC_MondialRelay_Configuration::get_champs_vendeur();
    $noty = 4;
}
// Reset Mondial Relay emails
elseif (isset($_GET['emails'])) {
    $champs = WC_MondialRelay_Configuration::get_champs_email();
    $noty = 5;
}
// Delete SQL parameters
if (isset($noty)) {
    WC_MondialRelay_Sql::reset_options_champs($champs);
    if ($noty == 1)
        WC_MondialRelay_Configuration::default_mondialrelay();
    elseif ($noty == 2)
        WC_MondialRelay_Configuration::default_configuration();
    elseif ($noty == 3)
        WC_MondialRelay_Configuration::default_points();
    elseif ($noty == 5)
        WC_MondialRelay_Configuration::default_email();
}

// Test Mondial Relay webservice (Search point - SOAP)
if (isset($_POST['webservice'])) {
    // Get Mondial Relay global parameters
    $mondialrelay_code_client = get_option( 'mondialrelay_code_client', 1 );
    $mondialrelay_cle_privee = get_option( 'mondialrelay_cle_privee', 1 );
    $client = new soapclient("http://www.mondialrelay.fr/WebService/Web_Services.asmx?WSDL");
    $params = array(
        'Enseigne'       => $mondialrelay_code_client,
        'Pays'         => 'FR',
        'Ville'		=> '',
        'CP'			=> '31000',
        'Poids' => '1000'
    );
    // Generation of the security key
    $code = implode("", $params);
    $code .= $mondialrelay_cle_privee;
    $params["Security"] = strtoupper(md5($code));
    $recherche = $client->WSI2_RecherchePointRelais($params)->WSI2_RecherchePointRelaisResult;
    $noty = 6;
    $callback = $recherche->STAT;
    // Status code
    if ($callback != 0)
        $libelle_retour = statut($callback);
    // Check required vendor's contact details
    elseif ($callback == 0) {
        // Get sender's parameters
        $params_expediteur = WC_MondialRelay_Order::get_params_expediteur();
        // Get addressee's parameters
        $params_vendeur = array('Enseigne'       => WC_MondialRelay::ENSEIGNE
        ,'ModeCol'        => WC_MondialRelay::MODE_COL
        ,'ModeLiv'        => WC_MondialRelay::MODE_LIV
        ,'Expe_Langage'   => $params_expediteur['mondialrelay_vendeur_langage']
        ,'Expe_Ad1'       => $params_expediteur['mondialrelay_vendeur_adresse1']
        ,'Expe_Ad3'       => $params_expediteur['mondialrelay_vendeur_adresse3']
        ,'Expe_Ad4'       => "Test"
        ,'Expe_Ville'     => $params_expediteur['mondialrelay_vendeur_ville']
        ,'Expe_CP'        => $params_expediteur['mondialrelay_vendeur_cp']
        ,'Expe_Pays'      => $params_expediteur['mondialrelay_vendeur_pays']
        ,'Expe_Tel1'      => $params_expediteur['mondialrelay_vendeur_tel']
        ,'Expe_Mail'      => $params_expediteur['mondialrelay_vendeur_email']
        ,'Dest_Langage'   => "FR"
        ,'Dest_Ad1'       => "Test Test"
        ,'Dest_Ad3'       => "Test"
        ,'Dest_Ville'     => "Test"
        ,'Dest_CP'        => "75000"
        ,'Dest_Pays'      => "FR"
        ,'Dest_Tel1'      => "0100000000"
        ,'Dest_Mail'      => "test@test.fr"
        ,'Poids'          => 1000
        ,'NbColis'        => '1'
        ,'CRT_Valeur'     => '0'
        ,'LIV_Rel_Pays'   => "FR"
        ,'LIV_Rel'        => "061956");
        // Generation of the security key
        $code = implode("", $params_vendeur);
        $code .= 'PrivateK';
        $params_vendeur["Security"] = strtoupper(md5($code));
        // Create the expedition
        $expedition = $client->WSI2_CreationExpedition($params_vendeur)->WSI2_CreationExpeditionResult;
        $vendeur = $expedition->STAT;
        // Status code
        if ($vendeur != 0)
            $libelle_vendeur_retour = statut($vendeur);
    }
}

// Check plugin's updates
$current = file_get_contents('http://www.mondialrelay-woocommerce.com/current-version');
$maj = 0;
if (version_compare( $this->version, $current, '<' ))
    $maj = 1;

// Debug mode
$mondialrelay_debug = get_option( 'mondialrelay_debug');
if (isset($_POST['debug'])) {
    update_option('mondialrelay_debug', $_POST['mondialrelay_debug']);
    $mondialrelay_debug = $_POST['mondialrelay_debug'];
    $noty = 7;
}

// Plugin position in the menu
$mondialrelay_menu_position = get_option( 'mondialrelay_menu_position');
if (isset($_POST['mondialrelay_position'])) {
    update_option('mondialrelay_menu_position', $_POST['mondialrelay_menu_position']);
    $mondialrelay_menu_position = $_POST['mondialrelay_menu_position'];
    $noty = 8;
}