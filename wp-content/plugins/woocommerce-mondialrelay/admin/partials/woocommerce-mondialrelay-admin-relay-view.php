<h2><?php _e("Point relais", 'woocommerce-mondialrelay') ?></h2>

<?php
if (isset($id_livraison_error) && $id_livraison_error == 1) { ?>
    <div class="error">
        <p><?php _e("<strong>Erreur :</strong> les identifiants de livraison Mondial Relay doivent seulement contenir des caractères alpha-numériques ainsi que les caractères \"_\" et \"-\" ", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php } ?>

<?php
if (isset($noty) && $noty == 1) { ?>
    <div class="updated">
        <p><?php _e("Les options du plugin Mondial Relay pour WooCommerce ont bien été mises à jour.", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php } ?>

<div id="message" class="updated woocommerce-message">
    <div class="squeezer">
        <p><strong><?php _e("Aide", 'woocommerce-mondialrelay') ?></strong></p>
        <p><?php _e("Pour plus d'informations sur les options du plugin, consultez", 'woocommerce-mondialrelay') ?> <a target="_blank" href="<?php echo WC_MondialRelay::DOCUMENTATION; ?>/points-relais"> <?php _e("la documentation", 'woocommerce-mondialrelay') ?></a>.</p>
    </div>
</div>

<h3><?php _e("Configuration de la recherche de points Mondial Relay", 'woocommerce-mondialrelay') ?></h3>
<form method="post" action="" id="mondialrelay_admin_recherche">
    <table class="form-table">
        <tbody>
        <?php foreach ($labels_points as $key => $label) { ?>
            <tr valign="top">
                <th scope="row"><label for="<?php echo $key ?>"><?php echo $label['label'] ?> </label></th>
                <td>
                    <?php if ($label['type'] == 'text') { ?>
                        <input name="<?php echo $key ?>" type="text" id="<?php echo $key ?>" value="<?php echo $champs[$key]; ?>" class="regular-text">
                    <?php } elseif ($label['type'] == 'radio') { ?>
                        <input type="radio" name="<?php echo $key ?>" id="<?php echo $key . '_oui' ?>" value="true" <?php if ($champs[$key] == 'true') echo 'checked'; ?>><label for="<?php echo $key . '_oui' ?>"><?php _e("Oui", 'woocommerce-mondialrelay') ?></label>
                        <input type="radio" name="<?php echo $key ?>" id="<?php echo $key . '_non' ?>" value="false" <?php if ($champs[$key] == 'false') echo 'checked'; ?>><label for="<?php echo $key . '_non' ?>"><?php _e("Non", 'woocommerce-mondialrelay') ?></label>
                    <?php } elseif ($label['type'] == 'select') { ?>
                        <select name="<?php echo $key ?>">
                            <option value=""> <?php _e("Choisir", 'woocommerce-mondialrelay') ?>
                                <?php
                                foreach($label['options'] as $cle => $option) { ?>
                            <option value="<?php echo $option ?>" <?php if ($champs[$key] == $option) echo 'selected'; ?>> <?php echo $cle ?>
                                <?php }
                                ?>
                        </select>
                    <?php } elseif ($label['type'] == 'multiple') { ?>
                        <select multiple size="11" name="<?php echo $key ?>[]">
                            <?php
                            foreach($label['options'] as $cle => $option) { ?>
                            <option value="<?php echo $option ?>" <?php if (WC_MondialRelay_Helpers::is_pays_autorise($old_pays_autorises, $option)) echo 'selected'; ?>> <?php echo $cle ?>
                                <?php }
                                ?>
                        </select>
                    <?php } ?>
                    <p class="description"><?php echo $label['description'] ?></p>
                </td>
            </tr>
        <?php } ?>
        <?php foreach ($labels_livraison as $key => $label) { ?>
            <tr valign="top">
                <th scope="row"><label for="<?php echo $key ?>"><?php echo $label['label'] ?> </label></th>
                <td>
                    <select name="<?php echo $key ?>">
                        <option value=""> <?php _e("Choisir", 'woocommerce-mondialrelay') ?>
                            <?php
                            foreach($label['options'] as $cle => $option) { ?>
                        <option value="<?php echo $option ?>" <?php if ($champs[$key] == $option) echo 'selected'; ?>> <?php echo $cle ?>
                            <?php }
                            ?>
                    </select>
                    <p class="description"><?php echo $label['description'] ?></p>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <hr>
    <h3><?php _e("Webmarketing", 'woocommerce-mondialrelay') ?></h3>
    <table class="form-table">
        <tbody>
        <?php foreach ($labels_webmarketing as $key => $label) {
            $champs[$key] = str_replace('\\', '', $champs[$key]);
            ?>
            <tr valign="top">
                <th scope="row"><label><?php echo $label['label']; ?> </label></th>
                <td>
                    <?php if ($label['type'] == 'textarea') { ?>
                        <textarea class="regular-text" name="<?php echo $key ?>"><?php echo $champs[$key];  ?></textarea>
                    <?php } ?>
                    <p class="description"><?php echo $label['description'] ?></p>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <hr>
    <h3><?php _e("Configuration spécifique du plugin", 'woocommerce-mondialrelay') ?></h3>
    <table class="form-table">
        <tbody>
        <?php foreach ($labels_specifiques as $key => $label) { ?>
            <tr valign="top">
                <th scope="row"><label><?php echo $label['label']; ?> </label></th>
                <td>
                    <?php if ($label['type'] == 'textarea') { ?>
                        <textarea class="regular-text" name="<?php echo $key ?>"><?php echo stripslashes($champs[$key]); ?></textarea>
                    <?php } ?>
                    <p class="description"><?php echo $label['description'] ?></p>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <hr>
    <h3><?php _e("Configuration du widget Parcelshop Picker de Mondial Relay", 'woocommerce-mondialrelay') ?></h3>
    <table class="form-table">
        <tbody>
        <?php foreach ($labels_widget as $key => $label) { ?>
            <tr valign="top">
                <th scope="row"><label><?php echo $label['label']; ?> </label></th>
                <td>
                    <select name="<?php echo $key ?>">
                        <option value=""> <?php _e("Choisir", 'woocommerce-mondialrelay') ?>
                            <?php
                            foreach($label['options'] as $option) { ?>
                        <option value="<?php echo $option ?>" <?php if ($champs[$key] == $option) echo 'selected'; ?>> <?php echo $option ?>
                            <?php }
                            ?>
                    </select>
                    <p class="description"><?php echo $label['description'] ?></p>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e("Enregistrer", 'woocommerce-mondialrelay') ?>"></p>

</form>