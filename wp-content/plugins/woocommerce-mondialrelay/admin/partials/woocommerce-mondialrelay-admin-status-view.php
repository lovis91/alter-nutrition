<h2><?php _e("Statut du plugin Mondial Relay", 'woocommerce-mondialrelay') ?></h2>
<br>

<?php
foreach($champs as $key => $champ) {
    ?>

    <table class="wc_status_table widefat" cellspacing="0">
        <thead>
        <tr>
            <th colspan="2"><strong><?php echo $champ ?></strong></th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($params[$key] as $cle => $meta) {
            $value = WC_MondialRelay_Sql::get_options_champ($meta);
            ?>
            <tr>
                <td><?php echo $meta ?></td>
                <td><?php echo $value ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

    <br>

<?php } ?>