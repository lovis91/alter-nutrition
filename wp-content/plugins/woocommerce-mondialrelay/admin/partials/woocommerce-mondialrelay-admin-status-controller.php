<?php

$champs = array('compte' => 'Paramètres de compte', 'points' => 'Paramètres des points relais', 'vendeur' => 'Paramètre du vendeur', 'email' => 'Paramètre des emails', 'autres' => 'Paramètres divers');

$params['compte'] = WC_MondialRelay_Configuration::get_champs_configuration();
$params['points'] = WC_MondialRelay_Configuration::get_champs_points();
$params['vendeur'] = WC_MondialRelay_Configuration::get_champs_vendeur();
$params['email'] = WC_MondialRelay_Configuration::get_champs_email();
$params['autres'] = WC_MondialRelay_Configuration::get_champs_autres();