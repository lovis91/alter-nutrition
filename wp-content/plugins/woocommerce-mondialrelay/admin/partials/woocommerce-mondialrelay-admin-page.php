<?php
    // COMPTE
    if (!isset($_GET['tab']) || $_GET['tab'] == 'compte')
        require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-compte-controller.php';
    // POINT RELAIS
    elseif ($_GET['tab'] == 'pointrelais')
        require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-relay-controller.php';
    // VENDEUR
    elseif ($_GET['tab'] == 'vendeur')
        require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-vendor-controller.php';
    // EMAIL
    elseif ($_GET['tab'] == 'email')
        require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-email-controller.php';

    // Display the page
    ?>
    <div class="wrap">

        <h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
            <a href="?page=wc-mondialrelay&amp;tab=compte" class="nav-tab <?php if (!isset($_GET['tab']) || $_GET['tab'] == 'compte') echo 'nav-tab-active'; ?>"><?php _e("Compte", 'woocommerce-mondialrelay') ?></a> <a href="?page=wc-mondialrelay&amp;tab=pointrelais" class="nav-tab <?php if (isset($_GET['tab']) && $_GET['tab'] == 'pointrelais') echo 'nav-tab-active'; ?>"><?php _e("Points Relais", 'woocommerce-mondialrelay') ?></a>	<a href="?page=wc-mondialrelay&amp;tab=vendeur" class="nav-tab <?php if (isset($_GET['tab']) && $_GET['tab'] == 'vendeur') echo 'nav-tab-active'; ?>"><?php _e("Vendeur", 'woocommerce-mondialrelay') ?></a> <a href="?page=wc-mondialrelay&amp;tab=email" class="nav-tab <?php if (isset($_GET['tab']) && $_GET['tab'] == 'email') echo 'nav-tab-active'; ?>"><?php _e("Emails", 'woocommerce-mondialrelay') ?></a></h2>
        <br>

        <?php
        // COMPTE
        if (!isset($_GET['tab']) || $_GET['tab'] == 'compte')
            require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-compte-view.php';
        // POINT RELAIS
        elseif ($_GET['tab'] == 'pointrelais')
            require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-relay-view.php';
        // VENDEUR
        elseif ($_GET['tab'] == 'vendeur')
            require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-vendor-view.php';
        // EMAIL
        elseif ($_GET['tab'] == 'email')
            require_once dirname( __FILE__ ) . '/woocommerce-mondialrelay-admin-email-view.php';
        ?>

    </div>