<?php

// Attributes forms
if (isset($_POST['mondialrelay_vendeur_adresse1']) && $_POST['mondialrelay_vendeur_adresse1'] != '')
    $mondialrelay_vendeur_adresse1 = $_POST['mondialrelay_vendeur_adresse1'];
else
    $mondialrelay_vendeur_adresse1 = '';
if (isset($_POST['mondialrelay_vendeur_adresse2']) && $_POST['mondialrelay_vendeur_adresse2'] != '')
    $mondialrelay_vendeur_adresse2 = $_POST['mondialrelay_vendeur_adresse2'];
else
    $mondialrelay_vendeur_adresse2 = '';
if (isset($_POST['mondialrelay_vendeur_adresse3']) && $_POST['mondialrelay_vendeur_adresse3'] != '')
    $mondialrelay_vendeur_adresse3 = $_POST['mondialrelay_vendeur_adresse3'];
else
    $mondialrelay_vendeur_adresse3 = '';
if (isset($_POST['mondialrelay_vendeur_cp']) && $_POST['mondialrelay_vendeur_cp'] != '')
    $mondialrelay_vendeur_cp = $_POST['mondialrelay_vendeur_cp'];
else
    $mondialrelay_vendeur_cp = '';
if (isset($_POST['mondialrelay_vendeur_ville']) && $_POST['mondialrelay_vendeur_ville'] != '')
    $mondialrelay_vendeur_ville = $_POST['mondialrelay_vendeur_ville'];
else
    $mondialrelay_vendeur_ville = '';
if (isset($_POST['mondialrelay_vendeur_pays']) && $_POST['mondialrelay_vendeur_pays'] != '')
    $mondialrelay_vendeur_pays = $_POST['mondialrelay_vendeur_pays'];
else
    $mondialrelay_vendeur_pays = '';
if (isset($_POST['mondialrelay_vendeur_tel']) && $_POST['mondialrelay_vendeur_tel'] != '')
    $mondialrelay_vendeur_tel = $_POST['mondialrelay_vendeur_tel'];
else
    $mondialrelay_vendeur_tel = '';
if (isset($_POST['mondialrelay_vendeur_email']) && $_POST['mondialrelay_vendeur_email'] != '')
    $mondialrelay_vendeur_email = $_POST['mondialrelay_vendeur_email'];
else
    $mondialrelay_vendeur_email = '';

// Save the attributes forms
$champs_vendeur = array('mondialrelay_vendeur_adresse1' => $mondialrelay_vendeur_adresse1, 'mondialrelay_vendeur_adresse2' => $mondialrelay_vendeur_adresse2, 'mondialrelay_vendeur_adresse3' => $mondialrelay_vendeur_adresse3, 'mondialrelay_vendeur_cp' => $mondialrelay_vendeur_cp, 'mondialrelay_vendeur_ville' => $mondialrelay_vendeur_ville, 'mondialrelay_vendeur_pays' => $mondialrelay_vendeur_pays, 'mondialrelay_vendeur_tel' => $mondialrelay_vendeur_tel, 'mondialrelay_vendeur_email' => $mondialrelay_vendeur_email);

// Remove accents
foreach($champs_vendeur as $key => $champ) {
    $champs_vendeur[$key] = remove_accents($champ);
}

// Labels of the Mondial Relay points attributes form
$labels_vendeur['mondialrelay_vendeur_adresse1']['label'] = __('Expéditeur', 'woocommerce-mondialrelay');
$labels_vendeur['mondialrelay_vendeur_adresse1']['obligatoire'] = 1;
$labels_vendeur['mondialrelay_vendeur_adresse2']['label'] = __('Expéditeur (Complément)', 'woocommerce-mondialrelay');
$labels_vendeur['mondialrelay_vendeur_adresse2']['obligatoire'] = 0;
$labels_vendeur['mondialrelay_vendeur_adresse3']['label'] = __('Rue', 'woocommerce-mondialrelay');
$labels_vendeur['mondialrelay_vendeur_adresse3']['obligatoire'] = 1;
$labels_vendeur['mondialrelay_vendeur_cp']['label'] = __('Code Postal', 'woocommerce-mondialrelay');
$labels_vendeur['mondialrelay_vendeur_cp']['obligatoire'] = 1;
$labels_vendeur['mondialrelay_vendeur_ville']['label'] = __('Ville', 'woocommerce-mondialrelay');
$labels_vendeur['mondialrelay_vendeur_ville']['obligatoire'] = 1;
$labels_vendeur['mondialrelay_vendeur_pays']['label'] = __("Pays", 'woocommerce-mondialrelay');
$labels_vendeur['mondialrelay_vendeur_pays']['options'] = WC_MondialRelay_Helpers::get_pays();
$labels_vendeur['mondialrelay_vendeur_pays']['type'] = "select";
$labels_vendeur['mondialrelay_vendeur_pays']['obligatoire'] = 1;
$labels_vendeur['mondialrelay_vendeur_tel']['label'] = __('Téléphone', 'woocommerce-mondialrelay');
$labels_vendeur['mondialrelay_vendeur_tel']['obligatoire'] = 1;
$labels_vendeur['mondialrelay_vendeur_email']['label'] = __('Adresse e-mail', 'woocommerce-mondialrelay');
$labels_vendeur['mondialrelay_vendeur_email']['obligatoire'] = 0;

// Update options
if (isset($_POST['submit'])) {
    $noty = 1;
    WC_MondialRelay_Sql::update_options_champs($champs_vendeur);
}

// Get the options
$champs_vendeur = WC_MondialRelay_Sql::get_options_champs($champs_vendeur);