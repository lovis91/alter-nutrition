<h2><?php _e("Compte Mondial Relay", 'woocommerce-mondialrelay') ?></h2>

<?php
if (isset($noty) && $noty == 1) { ?>
    <div class="updated">
        <p><?php _e("Les options du plugin Mondial Relay pour WooCommerce ont bien été mises à jour.", 'woocommerce-mondialrelay') ?></p>
    </div>
<?php } ?>

<div id="message" class="updated woocommerce-message">
    <div class="squeezer">
        <p><strong><?php _e("Aide", 'woocommerce-mondialrelay') ?></strong></p>
        <p><?php _e("Pour pouvoir créer des expéditions via Mondial Relay, l'inscription au service Mondial Relay est obligatoire afin d'obtenir votre code client + clé privée.", 'woocommerce-mondialrelay') ?><br>
            <?php _e("Si vous n’avez pas encore souscrit de compte professionnel Mondial Relay, vous pouvez en faire la demande à la société Mondial Relay via leur ", 'woocommerce-mondialrelay') ?> <a href="<?php echo WC_MondialRelay::MONDIALRELAY_INSCRIPTION ?>" target="_blank"><?php _e("formulaire de contact", 'woocommerce-mondialrelay') ?></a>.</p>
        <p><?php _e("Pour plus d'informations sur les options du plugin, consultez", 'woocommerce-mondialrelay') ?> <a target="_blank" href="<?php echo WC_MondialRelay::DOCUMENTATION; ?>/configuration"> <?php _e("la documentation", 'woocommerce-mondialrelay') ?></a>.</p>
    </div>
</div>

<h3><?php _e("Configuration du compte Mondial Relay", 'woocommerce-mondialrelay') ?></h3>

<form method="post" action="" id="mondialrelay_admin_compte">

    <table class="form-table">
        <tbody>
        <?php foreach ($labels_compte as $key => $label) { ?>
            <tr valign="top">
                <th scope="row"><label for="<?php echo $key ?>"><?php echo $label['label'] ?> </label></th>
                <td><input name="<?php echo $key ?>" type="text" id="<?php echo $key ?>" value="<?php echo $champs[$key]; ?>" class="regular-text">
                    <p class="description"><?php echo $label['description'] ?></p>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <hr>

    <h3><?php _e("Configuration de la livraison Mondial Relay", 'woocommerce-mondialrelay') ?></h3>

    <form method="post" action="">

        <table class="form-table">
            <tbody>
            <?php foreach ($labels_livraison as $key => $label) { ?>
                <tr valign="top">
                    <th scope="row"><label for="<?php echo $key ?>"><?php echo $label['label'] ?> </label></th>
                    <td>
                        <select name="<?php echo $key ?>">
                            <option value=""> Choisir
                                <?php
                                foreach($label['options'] as $cle => $option) { ?>
                            <option value="<?php echo $option ?>" <?php if ($champs[$key] == $option) echo 'selected'; ?>> <?php echo $cle ?>
                                <?php }
                                ?>
                        </select>
                        <p class="description"><?php echo $label['description'] ?></p>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e("Enregistrer", 'woocommerce-mondialrelay') ?>"></p>

    </form>