(function( $ ) {
	'use strict';

	jQuery(document).ready(function () {

		/**
		 * Account
		 * @type {Object}
		 */
		jQuery('#mondialrelay_admin_compte').validate({
			rules: {
				mondialrelay_code_client: {
					required: true,
					minlength: 8
				},
				mondialrelay_cle_privee: {
					required: true
				},
				mondialrelay_mode_colis: {
					required: true
				},
				mondialrelay_mode_livraison: {
					required: true
				},
				mondialrelay_assurance: {
					required: true
				}
			},
			messages: {
				mondialrelay_code_client: {
					required: " < Entrez votre code client",
					maxlength: " < Votre code client doit contenir au maximum 8 caractères"
				},
				mondialrelay_cle_privee: {
					required: " < Entrez votre clé privé"
				},
				mondialrelay_mode_colis: {
					required: " < Entrez votre mode de collecte"
				},
				mondialrelay_mode_livraison: {
					required: " < Entrez votre mode de livraison"
				},
				mondialrelay_assurance: {
					required: " < Entrez votre niveau d'assurance"
				}
			}
		});

		/**
		 * Relay search
		 * @type {Object}
		 */
		jQuery('#mondialrelay_admin_recherche').validate({
			rules: {
				mondialrelay_id_livraison: {
					required: true
				},
				mondialrelay_pays_livraison: {
					required: true
				},
				mondialrelay_code_postal: {
					minlength: 5,
					maxlength: 5
				},
				mondialrelay_geolocalisation: {
					required: true
				},
				mondialrelay_map: {
					required: true
				},
				mondialrelay_zoom: {
					required: true
				},
				mondialrelay_street_view: {
					required: true
				},
				mondialrelay_ssl: {
					required: true
				},
				mondialrelay_search_delay: {
					digits: true
				},
				mondialrelay_search_far: {
					digits: true
				},
				mondialrelay_nb_results: {
					digits: true
				},
				mondialrelay_mode_recherche: {
					required: true
				},
				mondialrelay_texte_selection: {
					required: true
				},
				mondialrelay_texte_aucun: {
					required: true
				},
				mondialrelay_texte_choix: {
					required: true
				},
				mondialrelay_texte_erreur: {
					required: true
				},
				mondialrelay_parcelshop_picker: {
					required: true
				}
			},
			messages: {
				mondialrelay_id_livraison: {
					required: " < Entrez vos méthodes de livraison"
				},
				mondialrelay_pays_livraison: {
					required: " < Sélectionnez le pays de livraison"
				},
				mondialrelay_code_postal: {
					minlength: " < Entrez un code postal valide",
					maxlength: " < Entrez un code postal valide"
				},
				mondialrelay_geolocalisation: {
					required: " < Choisissez votre option de géolocalisation"
				},
				mondialrelay_map: {
					required: " < Choisissez votre affichage"
				},
				mondialrelay_zoom: {
					required: " < Choisissez d'afficher ou non le zoom"
				},
				mondialrelay_street_view: {
					required: " < Choisissez d'afficher ou non la street view"
				},
				mondialrelay_ssl: {
					required: " < Choisissez d'activer ou non le SSL"
				},
				mondialrelay_search_delay: {
					digits: " < Entrez un nombre de jours valide"
				},
				mondialrelay_search_far: {
					digits: " < Entrez une distance valide"
				},
				mondialrelay_nb_results: {
					digits: " < Entrez un nombre valide"
				},
				mondialrelay_mode_recherche: {
					required: " < Sélectionnez un mode de recherche"
				},
				mondialrelay_texte_selection: {
					required: " < Entrez votre texte de sélection de point-relais"
				},
				mondialrelay_texte_aucun: {
					required: " < Entrez votre texte si aucun point-relais n'est sélectionné"
				},
				mondialrelay_texte_choix: {
					required: " < Entrez votre texte du bouton de sélection de point-relais"
				},
				mondialrelay_texte_erreur: {
					required:  " < Entrez votre texte en cas de non-sélection de point-relais"
				},
				mondialrelay_parcelshop_picker: {
					required: " < Choisissez la version du widget ParcelShop Picker"
				}
			}
		});

		/**
		 * Vendor
		 * @type {Object}
		 */
		jQuery('#mondialrelay_admin_vendeur').validate({
			rules: {
				mondialrelay_vendeur_adresse1: {
					required: true,
					maxlength: 32
				},
				mondialrelay_vendeur_adresse2: {
					maxlength: 32
				},
				mondialrelay_vendeur_adresse3: {
					required: true,
					maxlength: 32
				},
				mondialrelay_vendeur_cp: {
					required: true,
					maxlength: 5
				},
				mondialrelay_vendeur_ville: {
					required: true,
					maxlength: 26
				},
				mondialrelay_vendeur_pays: {
					required: true,
					maxlength: 2
				},
				mondialrelay_vendeur_tel: {
					required: true,
					maxlength: 13
				},
				mondialrelay_vendeur_email: {
					maxlength: 70
				}
			},
			messages: {
				mondialrelay_vendeur_adresse1: {
					required: " < Entrez votre nom d'expéditeur",
					maxlength: " < Votre nom d'expéditeur doit contenir au maximum 32 caractères"
				},
				mondialrelay_vendeur_adresse2: {
					maxlength: " < Votre complément d'expéditeur doit contenir au maximum 32 caractères"
				},
				mondialrelay_vendeur_adresse3: {
					required: " < Entrez votre rue",
					maxlength: " < Votre rue doit contenir au maximum 32 caractères"
				},
				mondialrelay_vendeur_cp: {
					required: " < Entrez votre code postal",
					maxlength: " < Votre code postal doit contenir au maximum 5 caractères"
				},
				mondialrelay_vendeur_ville: {
					required: " < Entrez votre ville",
					maxlength: " < Votre ville doit contenir au maximum 26 caractères"
				},
				mondialrelay_vendeur_pays: {
					required: " < Entrez votre pays",
					maxlength: " < Votre pays doit contenir au maximum 2 caractères"
				},
				mondialrelay_vendeur_tel: {
					required: " < Entrez votre numéro de téléphone",
					maxlength: " < Votre numéro de téléphone doit contenir au maximum 13 caractères"
				},
				mondialrelay_vendeur_email: {
					maxlength: " < Votre adresse e-mail doit contenir au maximum 70 caractères"
				},
			}
		});

		/**
		 * Emails
		 * @type {Object}
		 */
		jQuery('#mondialrelay_admin_emails').validate({
			rules: {
				sujet_expedition: {
					required: true
				},
				email_expedition: {
					required: true
				}
			},
			messages: {
				sujet_expedition: {
					required: " ^ Entrez un sujet"
				},
				email_expedition: {
					required: " ^ Entrez le contenu de votre email"
				}
			}
		});

		/**
		 * Debug
		 * @type {Object}
		 */
		jQuery('#mondialrelay_admin_debug').validate({
			rules: {
				mondialrelay_debug: {
					required: true
				}
			},
			messages: {
				mondialrelay_debug: {
					required: " < Activez ou non le mode debug"
				}
			}
		});

		/**
		 * Menu position
		 * @type {Object}
		 */
		jQuery('#mondialrelay_admin_menu_position').validate({
			rules: {
				mondialrelay_menu_position: {
					required: true
				}
			},
			messages: {
				mondialrelay_menu_position: {
					required: " < Choisissez la position du menu du plugin Mondial Relay"
				}
			}
		});

	});


})( jQuery );
