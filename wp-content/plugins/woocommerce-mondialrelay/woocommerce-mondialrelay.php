<?php

/**
 *
 * @link              http://www.mondialrelay-woocommerce.com
 * @since             1.0.0
 * @package           WC_MondialRelay
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce Mondial Relay
 * Plugin URI:        http://www.mondialrelay-woocommerce.com
 * Description:       Plugin Mondial Relay pour WooCommerce
 * Version:           3.0.9
 * Author:            Clément Barbaza
 * Author URI:        http://www.clementbarbaza.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce-mondialrelay
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Minimal version of PHP for the plugin
$versionPhpMin = '5.3.0';

/**
 * The code that runs during plugin activation.
 */
function activate_woocommerce_mondialrelay() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-mondialrelay-activator.php';
	WC_MondialRelay_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 */
/*
function deactivate_woocommerce_mondialrelay() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-mondialrelay-deactivator.php';
	WC_MondialRelay_Deactivator::deactivate();
}
*/

register_activation_hook( __FILE__, 'activate_woocommerce_mondialrelay' );
//register_deactivation_hook( __FILE__, 'deactivate_woocommerce_mondialrelay' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-mondialrelay.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function run_woocommerce_mondialrelay() {
	$plugin = new WC_MondialRelay();
	$plugin->run();

}

// Check PHP version of the server
require_once plugin_dir_path( __FILE__ ) . 'includes/wp-update-php.php';
$updatePhp = new WPUpdatePhp($versionPhpMin);
if ($updatePhp->does_it_meet_required_php_version(PHP_VERSION)) {
	run_woocommerce_mondialrelay();
}
