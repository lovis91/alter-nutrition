<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.mondialrelay-woocommerce.com
 * @since      1.0.0
 *
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 * @author     Clément Barbaza <contact@mondialrelay-woocommerce.com>
 */
class WC_MondialRelay_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
