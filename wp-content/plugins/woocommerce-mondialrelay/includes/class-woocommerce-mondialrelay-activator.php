<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.mondialrelay-woocommerce.com
 * @since      1.0.0
 *
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 * @author     Clément Barbaza <contact@mondialrelay-woocommerce.com>
 */
class WC_MondialRelay_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
		$prefixe = $wpdb->prefix;

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-mondialrelay-sql.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-mondialrelay-configuration.php';

		// Get Mondial Relay default params
		$champs = WC_MondialRelay_Configuration::get_champs_mondialrelay();
		// Create default attributes for Mondial Relay options
		WC_MondialRelay_Sql::insert_options_champs($champs);
	}

}
