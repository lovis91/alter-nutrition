<?php

/**
 * Orders
 *
 * @since      1.0.0
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 * @author     Clément Barbaza <contact@mondialrelay-woocommerce.com>
 */

class WC_MondialRelay_Order {

/**
 * Get the Mondial Relay orders
 * @param  $limit_start
 * @param  $pagination
 * @return $orders
 */
public static function get_orders($limit_start, $pagination) {
    global $wpdb;
    $prefixe = $wpdb->prefix;

    $orders = $wpdb->get_results( "SELECT ID, DATE_FORMAT(post_date, '%d/%m/%Y') as datefr, post_status FROM " . $prefixe . "posts WHERE post_type ='shop_order' ORDER BY ID DESC LIMIT " . $limit_start . "," . $pagination);
    return $orders;
}

/**
 * Get SQL checkout attributes
 * @return $params
 */
public static function get_params_checkout() {
    $params = array(
        'mondialrelay_code_client',
        'mondialrelay_id_livraison',
        'mondialrelay_pays_livraison',
        'mondialrelay_pays_autorises',
        'mondialrelay_code_postal',
        'mondialrelay_geolocalisation',
        'mondialrelay_ssl',
        'mondialrelay_map',
        'mondialrelay_street_view',
        'mondialrelay_zoom',
        'mondialrelay_search_delay',
        'mondialrelay_search_far',
        'mondialrelay_nb_results',
        'mondialrelay_mode_recherche',
        'mondialrelay_texte_aucun',
        'mondialrelay_texte_selection',
        'mondialrelay_texte_erreur'
    );
    return $params;
}

/**
 * Get the Mondial Relay account attributes
 * @return $params_compte
 */
public static function get_params_compte() {
    global $wpdb;
    $prefixe = $wpdb->prefix;

    $params_compte = array(
        'mondialrelay_code_client' => '',
        'mondialrelay_cle_privee' => ''
    );
    foreach($params_compte as $key => $param)
        $params_compte[$key] = get_option( $key, 1 );
    return $params_compte;
}

/**
 * Get the Mondial Relay expediteur attributes
 * @return $params_expediteur
 */
public static function get_params_expediteur() {
    global $wpdb;
    $prefixe = $wpdb->prefix;

    $params_expediteur = array(
        'mondialrelay_vendeur_adresse1' => '',
        'mondialrelay_vendeur_adresse2' => '',
        'mondialrelay_vendeur_adresse3' => '',
        'mondialrelay_vendeur_cp' => '',
        'mondialrelay_vendeur_ville' => '',
        'mondialrelay_vendeur_pays' => '',
        'mondialrelay_vendeur_tel' => '',
        'mondialrelay_vendeur_email' => ''
    );
    foreach($params_expediteur as $key => $param)
        $params_expediteur[$key] = get_option($key, 1);
    $params_expediteur['mondialrelay_vendeur_langage'] = WC_MondialRelay_Helpers::get_langue($params_expediteur['mondialrelay_vendeur_pays']);
    $params_expediteur['mondialrelay_vendeur_adresse1'] = remove_accents($params_expediteur['mondialrelay_vendeur_adresse1']);
    $params_expediteur['mondialrelay_vendeur_adresse2'] = remove_accents($params_expediteur['mondialrelay_vendeur_adresse2']);
    $params_expediteur['mondialrelay_vendeur_adresse3'] = remove_accents($params_expediteur['mondialrelay_vendeur_adresse3']);
    $params_expediteur['mondialrelay_vendeur_ville'] = remove_accents($params_expediteur['mondialrelay_vendeur_ville']);
    return $params_expediteur;
}

/**
 * Get the Mondial Relay destinataire attributes
 * @return $params_destinataire
 */
public static function get_params_destinataire($order_id) {
    global $wpdb;
    $prefixe = $wpdb->prefix;

    $params_destinataire = array(
        '_billing_last_name' => '',
        '_billing_first_name' => '',
        '_billing_email' => '',
        '_billing_phone' => '',
        '_shipping_address_1' => '',
        '_shipping_address_2' => '',
        '_shipping_city' => '',
        '_shipping_postcode' => '',
        '_shipping_country' => ''
    );
    foreach($params_destinataire as $key => $param) {
        $params_destinataire[$key] = $wpdb->get_var( "SELECT meta_value FROM " . $prefixe . "postmeta WHERE post_id = '" . $order_id . "' AND meta_key = '$key'" );
    }
    $params_destinataire['_shipping_langage'] = WC_MondialRelay_Helpers::get_langue($params_destinataire['_shipping_country']);
    $params_destinataire['_billing_nom'] = $params_destinataire['_billing_last_name'] . ' ' . $params_destinataire['_billing_first_name'];
    $params_destinataire['_billing_nom'] = remove_accents($params_destinataire['_billing_nom']);
    return $params_destinataire;
}

/**
 * Get the Mondial Relay order attributes
 * @return $params_commande
 */
public static function get_params_commande() {
    global $wpdb;
    $prefixe = $wpdb->prefix;

    $params_commande = array(
        'ModeCol' => WC_MondialRelay::MODE_COL,
        'ModeLiv' => WC_MondialRelay::MODE_LIV,
        'Poids' => '',
        'NbColis' => WC_MondialRelay::NB_COLIS,
        'CRT_Valeur' => WC_MondialRelay::CRT_VALEUR
    );
    foreach($params_commande as $key => $param) {
        if ($_POST[$key] != '')
            $params_commande[$key] = $_POST[$key];
        else {
            if ($key == 'ModeCol')
                $params_commande['ModeCol'] = $wpdb->get_var( "SELECT option_value FROM " . $prefixe . "options WHERE option_name = 'mondialrelay_mode_colis'" );
            elseif ($key == 'ModeLiv')
                $params_commande['ModeLiv'] = $wpdb->get_var( "SELECT option_value FROM " . $prefixe . "options WHERE option_name = 'mondialrelay_mode_livraison'" );
        }
    }
    return $params_commande;
}

/**
 * Get the Mondial Relay delivery attributes
 * @return $params_livraison
 */
public static function get_params_livraison() {
    $params_livraison = array(
        'liv_rel_pays_mondial_relay' => '',
        'liv_rel_mondial_relay' => ''
    );
    return $params_livraison;
}

/**
 * Get the Mondial Relay collect attributes
 * @return $params_collecte
 */
public static function get_params_collecte() {
    $params_collecte = array(
        'col_rel_pays_mondial_relay' => '',
        'col_rel_mondial_relay' => ''
    );
    return $params_collecte;
}

/**
 * Get SQL expedition attributes
 * @param  $expedition
 * @param $nb_colis
 * @return $champs
 */
public static function get_expedition_champs($expedition, $nb_colis) {
    $champs = array(
        'ExpeditionNum' => $expedition->ExpeditionNum,
        'TRI_AgenceCode' => $expedition->TRI_AgenceCode,
        'TRI_Groupe' => $expedition->TRI_Groupe,
        'TRI_Navette' => $expedition->TRI_Navette,
        'TRI_Agence' => $expedition->TRI_Agence,
        'TRI_TourneeCode' => $expedition->TRI_TourneeCode,
        'TRI_LivraisonMode' => $expedition->TRI_LivraisonMode
    );
    // Add the bar code
    if ($nb_colis == 1) {
        $champs['CodesBarres'] = $expedition->CodesBarres->string;
    }
    elseif ($nb_colis > 1) {
        $i = 0;
        while ($i < $nb_colis) {
            $champs['CodesBarres_' . $i] = $expedition->CodesBarres->string[$i];
            $i++;
        }
    }
    return $champs;
}

/**
 * Get SQL emails attributes
 * @param  $order_id
 * @return $email
 */
public static function get_params_emails($order_id) {
    $email['message'] = get_option( 'mondialrelay_email_expedition_message', 1 );
    if (!$email['message'])
        $email['message'] = email_expedition_message_default();
    $email['sujet'] = get_option( 'mondialrelay_email_expedition_sujet', 1 );
    if (!$email['sujet'])
        $email['sujet'] = email_expedition_sujet_default();
    $email['message'] = WC_MondialRelay_Helpers::parse_shortcodes($order_id, $email['message']);
    $email['sujet'] = WC_MondialRelay_Helpers::parse_shortcodes($order_id, $email['sujet']);
    return $email;
}

}