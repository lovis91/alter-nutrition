<?php

/**
 * Configuration of the plugin.
 *
 * @since      1.0.0
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 * @author     Clément Barbaza <contact@mondialrelay-woocommerce.com>
 */

class WC_MondialRelay_Configuration {

    /**
     * Get default attributes for Mondial Relay plugin
     * @return $champs
     */
    public static function get_champs_mondialrelay() {
        $champs = array(
            'mondialrelay_code_client' => WC_MondialRelay::ENSEIGNE,
            'mondialrelay_cle_privee' => WC_MondialRelay::CLE_PRIVEE,
            'mondialrelay_mode_colis' => WC_MondialRelay::MODE_COL,
            'mondialrelay_mode_livraison' => WC_MondialRelay::MODE_LIV,
            'mondialrelay_assurance' => WC_MondialRelay::ASSURANCE,
            'mondialrelay_id_livraison' => '',
            'mondialrelay_pays_livraison' => WC_MondialRelay::EXPE_PAYS,
            'mondialrelay_code_postal' => '',
            'mondialrelay_geolocalisation' => WC_MondialRelay::GEOLOCALISATION,
            'mondialrelay_pays_autorises' => '',
            'mondialrelay_ssl' => 'false',
            'mondialrelay_map' => 'true',
            'mondialrelay_street_view' => 'false',
            'mondialrelay_zoom' => 'false',
            'mondialrelay_search_delay' => '',
            'mondialrelay_search_far' => WC_MondialRelay::SEARCH_FAR,
            'mondialrelay_nb_results' => WC_MondialRelay::NB_RESULTS,
            'mondialrelay_style' => '',
            'mondialrelay_mode_recherche' => WC_MondialRelay::MODE_RECHERCHE,
            'mondialrelay_texte_selection' => __("Cliquez ici pour choisir un point de livraison Mondial Relay", 'woocommerce-mondialrelay'),
            'mondialrelay_texte_aucun' => __("Vous n\'avez pas sélectionné de point Mondial Relay", 'woocommerce-mondialrelay'),
            'mondialrelay_texte_choix' => __("Choisir ce point Mondial Relay", 'woocommerce-mondialrelay'),
            'mondialrelay_texte_erreur' => __("Veuillez sélectionner un point relais", 'woocommerce-mondialrelay'),
            'mondialrelay_email_expedition_sujet' => __("Votre commande n°[expedition] a été expédiée.", 'woocommerce-mondialrelay'),
            'mondialrelay_email_expedition_message' => __("Bonjour [prenom] [nom],<br /><br />Nous vous informons que votre commande a été expédiée. Vous la recevrez sous peu, mais si vous souhaitez garder un œil dessus, vous pouvez vous rendre sur [lien] en indiquant le numéro [expedition] pour suivre son trajet.<br /><br />Si nous pouvons vous être utiles de quelque manière que ce soit, n\'hésitez pas à nous le faire savoir.<br /><br />Votre service client", 'woocommerce-mondialrelay'),
            'mondialrelay_debug' => '0',
            'mondialrelay_menu_position' => WC_MondialRelay::MENU_POSITION,
            'mondialrelay_vendeur_adresse1' => '',
            'mondialrelay_vendeur_adresse2' => '',
            'mondialrelay_vendeur_adresse3' => '',
            'mondialrelay_vendeur_cp' => '',
            'mondialrelay_vendeur_ville' => '',
            'mondialrelay_vendeur_pays' => '',
            'mondialrelay_vendeur_tel' => '',
            'mondialrelay_vendeur_email' => '',
            'mondialrelay_parcelshop_picker' => WC_MondialRelay::PARCELSHOP_PICKER
        );
        return $champs;
    }

    /**
     * Get attributes for Mondial Relay plugin's configuration
     * @return $champs
     */
    public static function get_champs_configuration() {
        $champs = array(
            'mondialrelay_code_client',
            'mondialrelay_cle_privee',
            'mondialrelay_mode_colis',
            'mondialrelay_mode_livraison',
            'mondialrelay_assurance'
        );
        return $champs;
    }

    /**
     * Get attributes for Mondial Relay plugin's point relais
     * @return $champs
     */
    public static function get_champs_points() {
        $champs = array(
            'mondialrelay_id_livraison',
            'mondialrelay_pays_livraison',
            'mondialrelay_pays_autorises',
            'mondialrelay_code_postal',
            'mondialrelay_geolocalisation',
            'mondialrelay_ssl',
            'mondialrelay_map',
            'mondialrelay_street_view',
            'mondialrelay_zoom',
            'mondialrelay_search_delay',
            'mondialrelay_search_far',
            'mondialrelay_nb_results',
            'mondialrelay_style',
            'mondialrelay_mode_recherche',
            'mondialrelay_texte_aucun',
            'mondialrelay_texte_selection',
            'mondialrelay_texte_choix',
            'mondialrelay_texte_erreur',
            'mondialrelay_parcelshop_picker'
        );
        return $champs;
    }

    /**
     * Get attributes for Mondial Relay plugin's vendor
     * @return $champs
     */
    public static function get_champs_vendeur() {
        $champs = array(
            'mondialrelay_vendeur_adresse1',
            'mondialrelay_vendeur_adresse2',
            'mondialrelay_vendeur_adresse3',
            'mondialrelay_vendeur_cp',
            'mondialrelay_vendeur_ville',
            'mondialrelay_vendeur_pays',
            'mondialrelay_vendeur_tel',
            'mondialrelay_vendeur_email'
        );
        return $champs;
    }

    /**
     * Get attributes for Mondial Relay plugin's email
     * @param $old
     * @return $champs
     */
    public static function get_champs_email($old = null) {
        if ($old)
            $champs = array(
                'mondialrelay_email_expedition_sujet' => $old['mondialrelay_email_expedition_sujet'],
                'mondialrelay_email_expedition_message' => $old['mondialrelay_email_expedition_message']
            );
        else
            $champs = array(
                'mondialrelay_email_expedition_sujet',
                'mondialrelay_email_expedition_message'
            );
        return $champs;
    }

    /**
     * Get other attributes for Mondial Relay plugin
     * @return $champs
     */
    public static function get_champs_autres() {
        $champs = array(
            'mondialrelay_debug',
            'mondialrelay_menu_position'
        );
        return $champs;
    }

    /**
     * Update default Mondial Relay content
     * @return 1
     */
    public static function default_mondialrelay() {
        $champs = get_champs_mondialrelay();
        return WC_MondialRelay_Sql::update_options_champs($champs);
    }

    /**
     * Update default point relais content
     * @return 1
     */
    public static function default_points() {
        $champs = array(
            'mondialrelay_id_livraison' => '',
            'mondialrelay_pays_livraison' => WC_MondialRelay::DEST_PAYS,
            'mondialrelay_code_postal' => '',
            'mondialrelay_geolocalisation' => WC_MondialRelay::GEOLOCALISATION,
            'mondialrelay_pays_autorises' => '',
            'mondialrelay_ssl' => WC_MondialRelay::SSL,
            'mondialrelay_map' => WC_MondialRelay::MAP,
            'mondialrelay_street_view' => WC_MondialRelay::STREET_VIEW,
            'mondialrelay_zoom' => WC_MondialRelay::ZOOM,
            'mondialrelay_search_delay' => '',
            'mondialrelay_search_far' => WC_MondialRelay::SEARCH_FAR,
            'mondialrelay_nb_results' => WC_MondialRelay::NB_RESULTS,
            'mondialrelay_style' => '',
            'mondialrelay_mode_recherche' => WC_MondialRelay::MODE_RECHERCHE,
            'mondialrelay_texte_selection' => __("Cliquez ici pour choisir un point de livraison Mondial Relay", 'woocommerce-mondialrelay'),
            'mondialrelay_texte_aucun' => __("Vous n'avez pas sélectionné de point Mondial Relay", 'woocommerce-mondialrelay'),
            'mondialrelay_texte_choix' => __("Choisir ce point Mondial Relay", 'woocommerce-mondialrelay'),
            'mondialrelay_texte_erreur' => __("Veuillez sélectionner un point relais", 'woocommerce-mondialrelay'),
            'mondialrelay_parcelshop_picker' => WC_MondialRelay::PARCELSHOP_PICKER
        );
        return WC_MondialRelay_Sql::update_options_champs($champs);
    }

    /**
     * Update default Mondial Relay account content
     * @return 1
     */
    public static function default_configuration() {
        $champs = array(
            'mondialrelay_code_client' => WC_MondialRelay::ENSEIGNE,
            'mondialrelay_cle_privee' => WC_MondialRelay::CLE_PRIVEE,
            'mondialrelay_mode_colis' => WC_MondialRelay::MODE_COL,
            'mondialrelay_mode_livraison' => WC_MondialRelay::MODE_LIV,
            'mondialrelay_assurance' => WC_MondialRelay::ASSURANCE
        );
        return WC_MondialRelay_Sql::update_options_champs($champs);
    }

    /**
     * Update default email content
     * @return 1
     */
    public static function default_email() {
        $champs = array(
            'mondialrelay_email_expedition_sujet' => __("Votre commande n°[expedition] a été expédiée.", 'woocommerce-mondialrelay'),
            'mondialrelay_email_expedition_message' => __("Bonjour [prenom] [nom],<br /><br />Nous vous informons que votre commande a été expédiée. Vous la recevrez sous peu, mais si vous souhaitez garder un œil dessus, vous pouvez vous rendre sur [lien] en indiquant le numéro [expedition] pour suivre son trajet.<br /><br />Si nous pouvons vous être utiles de quelque manière que ce soit, n\'hésitez pas à nous le faire savoir.<br /><br />Votre service client", 'woocommerce-mondialrelay')
        );
        return WC_MondialRelay_Sql::update_options_champs($champs);
    }

    /**
     * Get email shortcodes
     * @return $shortcodes
     */
    public static function get_shortcodes_email($order_id) {
        $shortcodes = array(
            'pays' => '',
            'lien' => '',
            'expedition' => '',
            'prenom' => '',
            'nom' => '',
            'adresse' => ''
        );
        $champs = array(
            'pays' => '_shipping_country',
            'expedition' => 'ExpeditionNum',
            'prenom' => '_shipping_first_name',
            'nom' => '_shipping_last_name',
            'adresse1' => '_shipping_address_1',
            'adresse2' => '_shipping_address_2',
            'cp' => '_shipping_postcode',
            'ville' => '_shipping_city'
        );
        foreach ($champs as $champ => $valeur)
            $shortcodes[$champ] = WC_MondialRelay_Sql::get_meta_champ($order_id, $valeur);
        return $shortcodes;
    }

    /**
     * Get order meta
     * @return $champs
     */
    public static function get_order_meta() {
        $champs = array(
            '_billing_last_name' => '',
            '_billing_first_name' => '',
            '_shipping_company' => '',
            '_shipping_address_1' => '',
            '_shipping_address_2' => '',
            '_shipping_city' => '',
            '_shipping_postcode' => '',
            '_shipping_country' => ''
        );
        return $champs;
    }

}
