<?php

/**
 * Helpers of the plugin.
 *
 * @since      1.0.0
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 * @author     Clément Barbaza <contact@mondialrelay-woocommerce.com>
 */

class WC_MondialRelay_Helpers
{

    /**
     * Pretty print of a var
     * @param $var
     */
    public static function varPrint($var)
    {
        echo '<pre>' . print_r($var, true) . '</pre>';
    }

    /**
     * Order status
     * @param  string $etat
     */
    public static function etat_commande($etat)
    {
        switch ($etat) {
            case 'wc-pending':
                return _x('Pending payment', 'Order status', 'woocommerce');
                break;
            case 'wc-processing':
                return _x('Processing', 'Order status', 'woocommerce');
                break;
            case 'wc-on-hold':
                return _x('On hold', 'Order status', 'woocommerce');
                break;
            case 'wc-completed':
                return _x('Completed', 'Order status', 'woocommerce');
                break;
            case 'wc-cancelled':
                return _x('Cancelled', 'Order status', 'woocommerce');
                break;
            case 'wwc-refunded':
                return _x('Refunded', 'Order status', 'woocommerce');
                break;
            case 'wc-failed':
                return _x('Failed', 'Order status', 'woocommerce');
                break;
        }
    }

    /**
     * Status code (error)
     */
    public static function statut($callback)
    {
        switch ($callback) {
            case 1:
                return __("Enseigne invalide", 'woocommerce-mondialrelay');
                break;
            case 2:
                return __("Numéro d'enseigne vide ou inexistant", 'woocommerce-mondialrelay');
                break;
            case 3:
                return __("Numéro de compte enseigne invalide", 'woocommerce-mondialrelay');
                break;
            case 5:
                return __("Numéro de dossier enseigne invalide", 'woocommerce-mondialrelay');
                break;
            case 7:
                return __("Numéro de client enseigne invalide", 'woocommerce-mondialrelay');
                break;
            case 8:
                return __("Mot de passe ou hashage invalide", 'woocommerce-mondialrelay');
                break;
            case 9:
                return __("Ville non reconnu ou non unique", 'woocommerce-mondialrelay');
                break;
            case 10:
                return __("Type de collecte invalide", 'woocommerce-mondialrelay');
                break;
            case 11:
                return __("Numéro de Relais de Collecte invalide", 'woocommerce-mondialrelay');
                break;
            case 12:
                return __("Pays de Relais de Collecte invalide", 'woocommerce-mondialrelay');
                break;
            case 13:
                return __("Type de livraison invalide", 'woocommerce-mondialrelay');
                break;
            case 14:
                return __("Numéro de Relais de livraison invalide", 'woocommerce-mondialrelay');
                break;
            case 15:
                return __("Pays de Relais de livraison invalide", 'woocommerce-mondialrelay');
                break;
            case 20:
                return __("Poids du colis invalide", 'woocommerce-mondialrelay');
                break;
            case 21:
                return __("Taille (Longueur + Hauteur) du colis invalide", 'woocommerce-mondialrelay');
                break;
            case 22:
                return __("Taille du colis invalide", 'woocommerce-mondialrelay');
                break;
            case 24:
                return __("Numéro d'expédition ou de suivi invalide", 'woocommerce-mondialrelay');
                break;
            case 26:
                return __("Temps de montage invalide", 'woocommerce-mondialrelay');
                break;
            case 27:
                return __("Mode de collecte ou de livraison invalide", 'woocommerce-mondialrelay');
                break;
            case 28:
                return __("Mode de collecte invalide", 'woocommerce-mondialrelay');
                break;
            case 29:
                return __("Mode de livraison invalide", 'woocommerce-mondialrelay');
                break;
            case 30:
                return __("Adresse (L1) invalide", 'woocommerce-mondialrelay');
                break;
            case 31:
                return __("Adresse (L2) invalide", 'woocommerce-mondialrelay');
                break;
            case 33:
                return __("Adresse (L3) invalide", 'woocommerce-mondialrelay');
                break;
            case 34:
                return __("Adresse (L4) invalide", 'woocommerce-mondialrelay');
                break;
            case 35:
                return __("Ville invalide", 'woocommerce-mondialrelay');
                break;
            case 36:
                return __("Code postal invalide", 'woocommerce-mondialrelay');
                break;
            case 37:
                return __("Pays invalide", 'woocommerce-mondialrelay');
                break;
            case 38:
                return __("Numéro de téléphone invalide", 'woocommerce-mondialrelay');
                break;
            case 39:
                return __("Adresse e-mail invalide", 'woocommerce-mondialrelay');
                break;
            case 40:
                return __("Paramètres manquants", 'woocommerce-mondialrelay');
                break;
            case 42:
                return __("Montant CRT invalide", 'woocommerce-mondialrelay');
                break;
            case 43:
                return __("Devise CRT invalide", 'woocommerce-mondialrelay');
                break;
            case 44:
                return __("Valeur du colis invalide", 'woocommerce-mondialrelay');
                break;
            case 45:
                return __("Devise de la valeur du colis invalide", 'woocommerce-mondialrelay');
                break;
            case 46:
                return __("Plage de numéro d'expédition épuisée", 'woocommerce-mondialrelay');
                break;
            case 47:
                return __("Nombre de colis invalide", 'woocommerce-mondialrelay');
                break;
            case 48:
                return __("Multi-Colis Relais interdit", 'woocommerce-mondialrelay');
                break;
            case 49:
                return __("Action invalide", 'woocommerce-mondialrelay');
                break;
            case 60:
                return __("Champ texte libre invalide (Ce code erreur n'est pas invalidant)", 'woocommerce-mondialrelay');
                break;
            case 61:
                return __("Top avisage invalide", 'woocommerce-mondialrelay');
                break;
            case 62:
                return __("Instruction de livraison invalide", 'woocommerce-mondialrelay');
                break;
            case 63:
                return __("Assurance invalide", 'woocommerce-mondialrelay');
                break;
            case 64:
                return __("Temps de montage invalide", 'woocommerce-mondialrelay');
                break;
            case 65:
                return __("Top rendez-vous invalide", 'woocommerce-mondialrelay');
                break;
            case 66:
                return __("Top reprise invalide", 'woocommerce-mondialrelay');
                break;
            case 67:
                return __("Latitude invalide", 'woocommerce-mondialrelay');
                break;
            case 68:
                return __("Longitude invalide", 'woocommerce-mondialrelay');
                break;
            case 69:
                return __("Code Enseigne invalide", 'woocommerce-mondialrelay');
                break;
            case 70:
                return __("Numéro de Point Relais invalide", 'woocommerce-mondialrelay');
                break;
            case 71:
                return __("Nature de point de vente non valide", 'woocommerce-mondialrelay');
                break;
            case 74:
                return __("Langue invalide", 'woocommerce-mondialrelay');
                break;
            case 78:
                return __("Pays de collecte invalide", 'woocommerce-mondialrelay');
                break;
            case 79:
                return __("Pays de livraison invalide", 'woocommerce-mondialrelay');
                break;
            case 80:
                return __("Code tracing : colis enregistré", 'woocommerce-mondialrelay');
                break;
            case 81:
                return __("Code tracing : colis en traitement chez Mondial Relay", 'woocommerce-mondialrelay');
                break;
            case 82:
                return __("Code tracing : colis livré", 'woocommerce-mondialrelay');
                break;
            case 83:
                return __("Code tracing : anomalie", 'woocommerce-mondialrelay');
                break;
            case 84:
                return __("(Réservé Code Tracing)", 'woocommerce-mondialrelay');
                break;
            case 85:
                return __("(Réservé Code Tracing)", 'woocommerce-mondialrelay');
                break;
            case 86:
                return __("(Réservé Code Tracing)", 'woocommerce-mondialrelay');
                break;
            case 87:
                return __("(Réservé Code Tracing)", 'woocommerce-mondialrelay');
                break;
            case 88:
                return __("(Réservé Code Tracing)", 'woocommerce-mondialrelay');
                break;
            case 89:
                return __("(Réservé Code Tracing)", 'woocommerce-mondialrelay');
                break;
            case 93:
                return __("Aucun élément retourné par le plan de tri<br>Si vous effecture une collecte ou une livraison en Point Relaus, vérifiez que les Point Relais soient bien disponibles. Si vous effectuez une livraison à domicile, il est probable que le code postal que vous avez indiquez n'existe pas.", 'woocommerce-mondialrelay');
                break;
            case 94:
                return __("Colis inexistant", 'woocommerce-mondialrelay');
                break;
            case 95:
                return __("Compte Enseigne non activé", 'woocommerce-mondialrelay');
                break;
            case 96:
                return __("Type d'enseigne incorrect en Base", 'woocommerce-mondialrelay');
                break;
            case 97:
                return __("Clé de sécurité invalide", 'woocommerce-mondialrelay');
                break;
            case 98:
                return __("Erreur générique (Paramètres invalides)<br>Cette erreur masque une autre erreur de la liste et ne peut se produire que dans le cas où le compte utlisé est en mode \"Production\"", 'woocommerce-mondialrelay');
                break;
            case 99:
                return __("Erreur générique du service<br>Cette erreur peut être dû à un problème technique du service.<br> Veuillez notifier cette erreur à Mondial Relay en précisant la date et l'heure de la requête ainsi que les paramètres envoyés afin d'effectuer une vérification.", 'woocommerce-mondialrelay');
                break;
            default:
                return __("Aucune information disponible sur cette erreur", 'woocommerce-mondialrelay');
                break;
        }
    }

    /**
     * Tracking link according the country
     * @param  string $pays Country
     */
    public static function lien_suivi($pays)
    {
        switch ($pays) {
            case 'FR':
                return '<a href="http://www.mondialrelay.fr/suivi-de-colis/">http://www.mondialrelay.fr/suivi-de-colis/</a>';
                break;
            case 'BE':
                return '<a href="http://www.mondialrelay.be/fr-be/suivi-de-colis/">http://www.mondialrelay.be/fr-be/suivi-de-colis/</a>';
                break;
            case 'ES':
                return '<a href="http://www.puntopack.es/seguir-mi-envío/">http://www.puntopack.es/seguir-mi-envío/</a>';
                break;
        }
        return 1;
    }

    /**
     * Is the country authorized
     * @param  $pays
     * @param  $selection
     * @return boolean
     */
    public static function is_pays_autorise($pays, $selection)
    {
        foreach ($pays as $p) {
            if ($selection == $p)
                return 1;
        }
        return 0;
    }

// Récupère les shortcodes pour les emails
    /**
     * Get the email shortcodes
     * @param  int $order_id
     */
    public static function shortcodes_email($order_id)
    {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        $shortcodes            =  WC_MondialRelay_Configuration::get_shortcodes_email($order_id);
        $shortcodes['lien']    = self::lien_suivi($shortcodes['pays']);
        $shortcodes['adresse'] = $shortcodes['adresse1'] . ' ';
        if ($shortcodes['adresse2'] != '')
            $shortcodes['adresse'] .= $shortcodes['adresse2'] . ' ';
        $shortcodes['adresse'] .= $shortcodes['cp'] . ' ' . $shortcodes['ville'];
        return $shortcodes;
    }

    /**
     * Emails shortcodes parseur
     * @param  int $order_id
     * @param  string $texte
     */
    public static function parse_shortcodes($order_id, $texte)
    {
        $shortcodes = self::shortcodes_email($order_id);
        foreach ($shortcodes as $shortcode => $valeur)
            $texte = str_replace('[' . $shortcode . ']', $valeur, $texte);
        $texte = stripslashes($texte);
        return $texte;
    }

    /**
     * Get the countries (2 numbers ISO Code)
     * @return $pays
     */
    public static function get_pays()
    {
        $pays = array(
            'France' => 'FR',
            'Belgique' => 'BE',
            'Luxembourg' => 'LU',
            'Espagne' => 'ES',
            'Allemagne' => 'DE',
            'Autriche' => 'AT',
            'Angleterre' => 'UK',
            'Italie' => 'IT',
            'Portugal' => 'PT',
            'Pays-Bas' => 'NL',
            'Islande' => 'IE'
        );
        return $pays;
    }

    /**
     * Get the lang in accordance with the country (2 numbers ISO Code)
     * @param  string $pays Country
     */
    public static function get_langue($pays)
    {
        switch ($pays) {
            case 'FR':
                return 'FR';
                break;
            case 'BE':
                return 'FR';
                break;
            case 'ES':
                return 'ES';
                break;
            case 'DE':
                return 'DE';
                break;
            case 'IT':
                return 'IT';
                break;
            case 'LU':
                return 'LU';
                break;
            case 'PT':
                return 'PT';
                break;
            case 'UK':
                return 'UK';
                break;
            case 'IE':
                return 'IE';
                break;
            case 'NL':
                return 'NL';
                break;
            case 'AT':
                return 'AT';
                break;
        }
        return 1;
    }

    /**
     * Convert weight in accordance with a unit in g
     * @param  int $poids
     */
    public static function convert_poids($poids)
    {
        $unit = get_option('woocommerce_weight_unit', 0);
        switch ($unit) {
            case 'kg':
                $poids = $poids * 1000;
                break;
            case 'lbs':
                $poids = $poids * 453.59237;
                break;
            case 'oz':
                $poids = $poids * 28.3495231;
                break;
        }
        // Round in an integer due to the Mondial Relay API
        $poids = round($poids, 0);
        return $poids;
    }

}