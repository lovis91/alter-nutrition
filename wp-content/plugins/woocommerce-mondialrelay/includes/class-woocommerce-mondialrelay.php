<?php

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 * @author     Clément Barbaza <contact@mondialrelay-woocommerce.com>
 */
class WC_MondialRelay {

	const PLUGIN_NAME = 'woocommerce-mondialrelay';
	const VERSION = '3.0.9';
    const CURRENT_VERSION = 'http://www.mondialrelay-woocommerce.com/current-version';
    const DOCUMENTATION = 'http://docs.mondialrelay-woocommerce.com';
	const CLIENT = 'https://client.mondialrelay-woocommerce.com';
	const MENU_POSITION = '58.1';
    const DEBUG = false;
    const DEV = false;

	const ENSEIGNE = 'BDTEST13';
    const CLE_PRIVEE = 'PrivateK';
    const MODE_COL = 'CCC';
    const MODE_LIV = '24R';
    const ASSURANCE = '0';
    const EXPE_PAYS = 'FR';
    const DEST_PAYS = 'FR';
    const GEOLOCALISATION = 'true';
    const SEARCH_FAR = '75';
    const NB_RESULTS = '7';
    const MODE_RECHERCHE = '24R';
    const PARCELSHOP_PICKER = '3.2';
    const SSL = 'false';
    const ZOOM = 'false';
    const MAP = 'true';
    const STREET_VIEW = 'false';
    const NB_COLIS = '1';
    const CRT_VALEUR = '0';

	const MONDIALRELAY_INSCRIPTION = 'http://www.mondialrelay.fr/solutions-pour-les-pros/contactez-nous/';

    // const in mondialrelay.js too

	/**
	 * Woocommerce versions compatible with the plugin
	 * @var array
	 */
	protected $wc_compatibles = array(
		'2.5.2', '2.5.1', '2.5.0',
		'2.4.13', '2.4.12', '2.4.11', '2.4.10', '2.4.9', '2.4.8', '2.4.7', '2.4.6', '2.4.5', '2.4.4', '2.4.3', '2.4.2', '2.4.1', '2.4.0',
		'2.3.13', '2.3.12', '2.3.11', '2.3.10', '2.3.9', '2.3.8', '2.3.7', '2.3.6', '2.3.5' ,'2.3.4', '2.3.3', '2.3.2', '2.3.1', '2.3.0',
		'2.2.11', '2.2.10', '2.2.9' , '2.2.8', '2.2.7', '2.2.6','2.2.5', '2.2.4', '2.2.3', '2.2.2', '2.2.1', '2.2.0',
		'2.1.12', '2.1.11', '2.1.10', '2.1.9', '2.1.8', '2.1.7', '2.1.6', '2.1.5', '2.1.4', '2.1.3', '2.1.2', '2.1.1', '2.1.0',
		'2.0.20',
		'1.6.6'
	);

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      WC_mondialRelay_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Dev mode
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $dev    The current version of the plugin.
	 */
	protected $dev;

	/**
	 * Debug
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $debug   The current version of the plugin.
	 */
	protected $debug;

	/**
	 * Menu position
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $menu_position    The current version of the plugin.
	 */
	protected $menu_position;

	/**
	 * Current version
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $current_version    The current version of the plugin.
	 */
	protected $current_version;

	/**
	 * Documentation
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $documentation    Documentation of the plugin
	 */
	protected $documentation;

	/**
	 * Client's account link
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $client    Client's account link
	 */
	protected $client;

	/**
	 * Is plugin compatible ?
	 *
	 * @access   protected
	 * @var boolean		$compatible 	compatibility of the plugin.
	 */
	protected $compatible;

	/**
	 * WooCommerce version
	 *
	 * @access   protected
	 * @var string 		$wc_version 	WooCommerce version.
	 */
	protected $wc_version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = self::PLUGIN_NAME;
		$this->version = self::VERSION;
		$this->dev = self::DEV;
		$this->debug = self::DEBUG;
		$this->menu_position = self::MENU_POSITION;
		$this->current_version = self::CURRENT_VERSION;
		$this->documentation = self::DOCUMENTATION;
		$this->client = self::CLIENT;

		$this->load_dependencies();
		$this->set_locale();

		// Check if the WooCommerce version is compatible with the plugin
		if ($this->compatible()) {
			// Get the debug mode
			$this->mode_debug();
			// Get the position plugin in the menu
			$this->menu_position();
            // Functions call for the administration of the plugin
            $this->define_public_hooks();
            if (is_admin()) {
                $this->define_admin_hooks();
                // nuSOAP library
                require_once dirname( __FILE__ ) . '/nusoap/nusoap.php';
                // Configuration functions
                require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-mondialrelay-sql.php';
                require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-mondialrelay-configuration.php';
                require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-mondialrelay-helpers.php';
                require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-mondialrelay-order.php';
            }
		}
		else {
			if (is_admin()) {
				add_action('admin_notices', array($this, 'notification_compatibilite'));
			}
		}

	}

	/**
	 * Get WooCommerce version
	 * @return [type] [description]
	 */
	public function version() {
		if (!function_exists('get_plugins'))
			require_once(ABSPATH . 'wp-admin/includes/plugin.php');
		$plugin_folder = get_plugins('/' . 'woocommerce');
		$plugin_file = 'woocommerce.php';
		if ( isset( $plugin_folder[$plugin_file]['Version'])) {
			$this->wc_version = $plugin_folder[$plugin_file]['Version'];
		} else {
			$this->wc_version = null;
		}
	}

	/**
	 * Check compatibility of the plugin with WooCommerce
	 */
	public function compatible() {
		// Get WooCommerce version
		$this->version();
		// Check if WooCommerce version is compatible
		if (in_array($this->wc_version, $this->wc_compatibles)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Notification function if WooCommerce version isn't compatible with the plugin
	 */
	public function notification_compatibilite() {

		echo "<div class='update-nag'>";
		echo '<h3>';
		_e("Plugin Mondial Relay pour WooCommerce", 'woocommerce-mondialrelay');
		echo '</h3>';
		echo '<h4 style="color: #ff0000">';
		_e("Problème de compatibilité", 'woocommerce-mondialrelay');
		echo '</h4>';
		echo $compatibilite_old = sprintf( __("Vous utilisez la version %s de WooCommerce qui n'est pas compatible avec votre version du plugin Mondial Relay pour WooCommerce.", 'woocommerce-mondialrelay'), $this->wc_version);
		echo '<br>';
		echo $compatibilite_old2 = sprintf( __("Le plugin Mondial Relay %s est seulement compatible avec des versions antérieures de WooCommerce ", 'woocommerce-mondialrelay'), $this->version );
		echo $compatibilite_old3 = sprintf( __('(<a href="%s/compatibilite" target="_blank">plus de détails</a>)', 'woocommerce-mondialrelay'), WC_MondialRelay::DOCUMENTATION);
		echo ".";
		// if WooCommerce is newer than the last version supported by the plugin
		if (version_compare( $this->wc_version, $this->wc_compatibles[0], '>' )) {
			echo '<p>';
			_e("Votre version de WooCommerce est plus récente que le plugin Mondial Relay pour WooCommerce.", 'woocommerce-mondialrelay');
			echo '<br>';
			echo $this->verif_maj();
			echo '</p>';
		}
		echo $compatibilite_old3 = sprintf( __('<a href="%s/maj" target="_blank">Plus d\'informations</a>', 'woocommerce-mondialrelay'), WC_MondialRelay::DOCUMENTATION);
		echo "</div>";
	}

	/**
	 * Check plugin updates
	 */
	public function verif_maj() {
		$current = file_get_contents($this->current_version);
		if (version_compare( $this->version, $current, '<' )) {
			return __("Votre version du plugin Mondial Relay pour WooCommerce n'est pas à jour.", 'woocommerce-mondialrelay') . ' ' . sprintf( __("La version %s de Mondial Relay pour WooCommerce est disponible en téléchargement.", 'woocommerce-mondialrelay'), $current) . "<br><br>" . "<a href='" . $this->client . "' target='_blank' class='button-primary'>" .  __("Veuillez mettre à jour le plugin Mondial Relay pour WooCommerce", 'woocommerce-mondialrelay') . "</a>";
		}
		else {
			return __("Votre version du plugin Mondial Relay pour WooCommerce est à jour. ", 'woocommerce-mondialrelay') .  __("Une mise à jour sera bientôt disponible en téléchargement.", 'woocommerce-mondialrelay');
		}
	}

	/**
	 * Get debug mode
	 */
	public function mode_debug() {
		$this->debug = get_option( 'mondialrelay_debug', 0 );
	}

	/**
	 * Get position plugin in the menu
	 */
	public function menu_position() {
		$this->menu_position = get_option( 'mondialrelay_menu_position', self::MENU_POSITION );
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - WC_MondialRelay_Loader. Orchestrates the hooks of the plugin.
	 * - WC_MondialRelay_i18n. Defines internationalization functionality.
	 * - WC_MondialRelay_Admin. Defines all hooks for the admin area.
	 * - WC_MondialRelay_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-mondialrelay-loader.php';
		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-mondialrelay-i18n.php';
		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-woocommerce-mondialrelay-admin.php';
		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-woocommerce-mondialrelay-public.php';
		$this->loader = new WC_MondialRelay_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the WC_MondialRelay_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {
		$plugin_i18n = new WC_MondialRelay_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {
		$plugin_admin = new WC_MondialRelay_Admin($this->get_plugin_name(), $this->get_version(), $this->get_current_version(), $this->get_menu_position());
		$this->loader->add_action('plugin_row_meta', $plugin_admin, 'plugin_row_meta', 10, 2);
        $this->loader->add_action('admin_menu', $plugin_admin, 'MondialRelayMenu');
        $this->loader->add_action('admin_menu', $plugin_admin, 'notificationMenu');
        $this->loader->add_action('add_meta_boxes', $plugin_admin, 'actions_mr_meta_box');
        $this->loader->add_action('admin_footer', $plugin_admin, 'expedition_javascript');
        $this->loader->add_action('wp_ajax_expedition', $plugin_admin, 'expedition_callback');
		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
	}

	/**
	 * Register all of the hooks related to the public area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
    public function define_public_hooks() {
        $plugin_public = new WC_MondialRelay_Public( $this->get_plugin_name(), $this->get_version(), $this->get_dev(), $this->get_debug() );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts', 9 );
        $this->loader->add_action( 'woocommerce_after_order_notes', $plugin_public, 'mondial_relay_fancybox' );
        $this->loader->add_action( 'woocommerce_after_order_notes', $plugin_public, 'my_custom_checkout_field' );
        $this->loader->add_action( 'woocommerce_checkout_order_review', $plugin_public, 'mondialrelay_params' );
    }

	/**
	 * Save Mondial Relay parameters
	 * @param  int $order_id
	 */
	public function my_custom_checkout_field_update_order_meta($order_id) {
		if ($_POST['id_mondial_relay']) update_post_meta($order_id, 'ID Point Mondial Relay', esc_attr($_POST['id_mondial_relay']));
		if ($_POST['infos_mondial_relay']) update_post_meta($order_id, 'Adresse Point Mondial Relay', esc_attr($_POST['infos_mondial_relay']));
	}

	/**
	 * Modifiy delivery address if Mondial Relay is selected
	 * @param  int $order_id
	 */
	public function mondial_relay_order($order_id) {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-mondialrelay-sql.php';
        $is_mondial_relay = WC_MondialRelay_Sql::get_meta_champ($order_id, 'Adresse Point Mondial Relay', 1);
		if (isset($is_mondial_relay) && $is_mondial_relay == 1) {
			$commande = WC_MondialRelay_Sql::get_meta_champ($order_id, 'Adresse Point Mondial Relay');
			$mondialrelay = explode(' - ', $commande);
			$l = count($mondialrelay);
			$mondialrelay_nom = $mondialrelay[0];
			$mondialrelay_adresse_1 = trim($mondialrelay[1]);
			// Complement address
			if ($l == 5) {
				$mondialrelay_adresse_2 = trim($mondialrelay[2]);
				$mondialrelay_pays = trim($mondialrelay[4]);
				$mondialrelay_pcv = trim($mondialrelay[3]);
			}
			// No complement address
			else if ($l == 4) {
				$mondialrelay_pcv = trim($mondialrelay[2]);
				$mondialrelay_adresse_2 = '';
				$mondialrelay_pays = trim($mondialrelay[3]);
			}
			$mondialrelay2 = explode(' ', $mondialrelay_pcv);
			$mondialrelay_cp = $mondialrelay2[0];
			$mondialrelay_ville = $mondialrelay2[1];
			update_post_meta($order_id, '_shipping_company', $mondialrelay_nom);
			update_post_meta($order_id, '_shipping_address_1', $mondialrelay_adresse_1);
			update_post_meta($order_id, '_shipping_address_2', $mondialrelay_adresse_2);
			update_post_meta($order_id, '_shipping_postcode', $mondialrelay_cp);
			update_post_meta($order_id, '_shipping_city', $mondialrelay_ville);
			update_post_meta($order_id, '_shipping_country', $mondialrelay_pays);
		}
	}

	/**
	 * Function called after loaded WooCommerce
	 */
	public function woocommerce_loaded() {
		// Save attributes
		add_action('woocommerce_checkout_update_order_meta', array($this , 'my_custom_checkout_field_update_order_meta'));
		// Modifiy delivery adress if Mondial Relay is selected
		add_action('woocommerce_checkout_update_order_meta', array($this , 'mondial_relay_order'));
	}

	/**
	 * Load scripts and styles when checkout page is active
	 * @return [type] [description]
	 */
	public function plugin_is_checkout() {
		if (is_checkout()) {
            require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-mondialrelay-order.php';
            $this->define_public_hooks();
		}
	}

	/**
	 * Function called after loaded all the plugin
	 */
	public function plugins_loaded() {
		// Load checkout functions
		add_action('template_redirect', array($this , 'plugin_is_checkout'), 9);
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		// Function called after loaded WooCommerce
		$this->loader->add_action('woocommerce_init', $this, 'woocommerce_loaded');
		// Function called after loaded all the plugin
		$this->loader->add_action('plugins_loaded', $this, 'plugins_loaded');
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Plugin_Name_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

    /**
     * Retrieve the current version number of the WooCommerce
     *
     * @since     1.0.0
     * @return    string    The version number of WooCommerce.
     */
    public function get_current_version() {
        return $this->current_version;
    }

    /**
     * Retrieve the menu position of the plugin
     *
     * @since     1.0.0
     * @return    string    The menu position of the plugin
     */
    public function get_menu_position() {
        return $this->menu_position;
    }

	/**
	 * Retrieve the documentation link of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The documentation link of the plugin
	 */
    public function get_documentation() {
        return $this->documentation;
    }

	/**
	 * Retrieve the mode of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The mode of the plugin
	 */
    public function get_dev() {
        return $this->dev;
    }

	/**
	 * Retrieve the debug mode of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The debug mode of the plugin
	 */
    public function get_debug() {
        return $this->debug;
    }


}
