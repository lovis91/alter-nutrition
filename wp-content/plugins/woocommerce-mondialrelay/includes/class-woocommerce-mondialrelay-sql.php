<?php

/**
 * SQL functions of the plugin.
 *
 * @since      1.0.0
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 * @author     Clément Barbaza <contact@mondialrelay-woocommerce.com>
 */

class WC_MondialRelay_Sql {

    /**
     * Get SQL options attributes
     * @param  $champs
     * @return $champs
     */
    public static function get_options_champs($champs) {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        foreach($champs as $champ => $valeur) {
            $champs[$champ] = $wpdb->get_var( "SELECT option_value FROM " . $prefixe . "options WHERE option_name = '$champ'" );
        }
        return $champs;
    }

    /**
     * Get SQL options attribute
     * @param  $champ
     * @return $champ
     */
    public static function get_options_champ($champ, $count = null) {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        if ($count)
            $champ = $wpdb->get_var( "SELECT COUNT(*) as total FROM " . $prefixe . "options WHERE option_name = '$champ'" );
        else
            $champ = $wpdb->get_var( "SELECT option_value FROM " . $prefixe . "options WHERE option_name = '$champ'" );
        return $champ;
    }

    /**
     * Insert SQL options attributes
     * @param  $champs
     * @return 1
     */
    public static function insert_options_champs($champs) {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        foreach($champs as $champ => $valeur) {
            $verif_champ = self::get_options_champ($champ, 1);
            if ($verif_champ == 0) {
                $wpdb->insert($prefixe . 'options', array(
                    'option_id'     => NULL,
                    'option_name'    => '' . $champ . '',
                    'option_value'   => '' . $valeur . '',
                    'autoload'    => 'yes'
                ));
            }
        }
        return 1;
    }

    /**
     * Update SQL options attributes
     * @param  $champs
     * @return 1
     */
    public static function update_options_champs($champs) {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        foreach($champs as $champ => $valeur) {
            $valeur = trim($valeur);
            $valeur = strip_tags(trim($valeur));
            update_option($champ, $valeur);
        }
        return 1;
    }

    /**
     * Reset SQL options attributes
     * @param  $champs
     * @return 1
     */
    public static function reset_options_champs($champs) {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        foreach($champs as $champ) {
            update_option($champ, '');
        }
        return 1;
    }

    /**
     * Delete SQL options attributes
     * @param  $champs
     * @return 1
     */
    public static function delete_options_champs($champs) {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        foreach($champs as $champ) {
            $exist = $wpdb->get_var( "SELECT COUNT(*) as total FROM " . $prefixe . "options WHERE option_name = '" . $champ . "'" );
            if ($exist > 0) {
                $wpdb->delete( $prefixe.'options', array( 'option_name' => $champ ) );
            }
        }
        return 1;
    }

    /**
     * Get SQL meta attributes
     * @param $id
     * @param $champs
     * @return 1
     */
    public static function get_meta_champs($id, $champs) {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        foreach($champs as $key => $champ) {
            $champs[$key] = $wpdb->get_var( "SELECT meta_value FROM " . $prefixe . "postmeta WHERE post_id ='$id' AND meta_key = '$key'");
        }
        return $champs;
    }

    /**
     * Get SQL meta attribute
     * @param $id
     * @param $key
     * @return $value
     */
    public static function get_meta_champ($id, $key, $count = null) {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        if ($count)
            $value = $wpdb->get_var( "SELECT COUNT(*) FROM " . $prefixe . "postmeta WHERE post_id ='$id' AND meta_key = '$key'");
        else
            $value = $wpdb->get_var( "SELECT meta_value FROM " . $prefixe . "postmeta WHERE post_id ='$id' AND meta_key = '$key'");
        return $value;
    }

    /**
     * Insert SQL meta attributes
     * @param $champs
     * @return 1
     */
    public static function insert_meta_champs($order_id, $champs) {
        global $wpdb;
        $prefixe = $wpdb->prefix;

        foreach ($champs as $key => $champ) {
            $wpdb->insert($prefixe . 'postmeta', array(
                'meta_id'     => NULL,
                'post_id'    => '' . $order_id . '',
                'meta_key'   => '' . $key . '',
                'meta_value'    => '' . $champ . ''
            ));
        }
        return 1;
    }

}
