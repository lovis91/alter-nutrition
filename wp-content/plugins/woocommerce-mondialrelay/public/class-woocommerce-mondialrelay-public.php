<?php

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 * @author     Clément Barbaza <contact@mondialrelay-woocommerce.com>
 */
class WC_MondialRelay_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Development mode
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $dev    Development mode.
	 */
    private $dev;

	/**
	 * Debug mode
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $debug    Debug mode.
	 */
    private $debug;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 * @param      string    dev    Development mode.
	 * @param      string    $debug    Debug mode.
	 */
	public function __construct( $plugin_name, $version, $dev, $debug ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
        $this->dev = $dev;
        $this->debug = $debug;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		// Load generals scripts/styles
		wp_enqueue_style('mondial-relay-theme-css', plugin_dir_url(dirname(__FILE__)).'public/css/style-theme.css');
		// Load Fancybox
		wp_enqueue_style('fancybox-css', plugin_dir_url(dirname(__FILE__)).'public/fancybox/jquery.fancybox.css?v=2.1.5');
		$this->mondial_relay_user_styles();
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
        // Google Maps
		wp_register_script('google-maps', 'https://maps.googleapis.com/maps/api/js?sensor=false', array('jquery'));
        wp_enqueue_script('google-maps');
		// Mondial Relay Parcelshop Picker
		$parcelshop_picker = get_option('mondialrelay_parcelshop_picker', WC_MondialRelay::PARCELSHOP_PICKER);
		$parcelshop_picker = str_replace('.', '_', $parcelshop_picker);
		$parcelshop_picker = 'v' . $parcelshop_picker;
		if ($this->dev) {
			wp_register_script('mondial-relay', plugin_dir_url(dirname(__FILE__)).'public/js/dev/parcelshop-picker/'.$parcelshop_picker.'/jquery.plugin.mondialrelay.parcelshoppicker.js?v='.$this->version, array( 'jquery', 'google-maps' ));
		}
		else {
			wp_register_script('mondial-relay', plugin_dir_url(dirname(__FILE__)).'public/js/parcelshop-picker/'.$parcelshop_picker.'/jquery.plugin.mondialrelay.parcelshoppicker.min.js?v='.$this->version, array( 'jquery', 'google-maps' ));
		}
		wp_enqueue_script('mondial-relay' );
		// Load Fancybox
		wp_register_script('fancybox', plugin_dir_url(dirname(__FILE__)).'public/fancybox/jquery.fancybox.js?v=2.1.5', array('jquery'));
		wp_enqueue_script('fancybox' );
		// Unload WooCommerce checkout file
		wp_dequeue_script('wc-checkout');
		// Load Mondial Relay script file
		if ($this->dev) {
			wp_register_script('mondialrelay', plugin_dir_url(dirname(__FILE__)).'public/js/dev/mondialrelay.js', array('jquery', 'woocommerce', 'google-maps', 'mondial-relay'));
		}
		else {
			wp_register_script('mondialrelay', plugin_dir_url(dirname(__FILE__)).'public/js/mondialrelay.min.js?v='.$this->version, array('jquery', 'woocommerce', 'google-maps', 'mondial-relay'));
		}
		wp_enqueue_script('mondialrelay');
		// Load the modify checjout.js file of WooCommerce in accordance with the version
		$v = explode('.', WOOCOMMERCE_VERSION);
		$wc_version = $v[0] . '-' . $v[1] . '-' . $v[2];
		// Load new checkout.js file
		if ($this->dev) {
			wp_register_script('wc-checkout', plugin_dir_url(dirname(__FILE__)).'public/js/dev/checkout/checkout-'.$wc_version.'.js', array('jquery', 'woocommerce', 'google-maps', 'mondial-relay','mondialrelay'));
		}
		else {
			wp_register_script('wc-checkout', plugin_dir_url(dirname(__FILE__)).'public/js/checkout/checkout-'.$wc_version.'.min.js', array('jquery', 'woocommerce', 'google-maps', 'mondial-relay','mondialrelay'));
		}
	}

    /**
     * Load user styles
     */
    public function mondial_relay_user_styles() {
        wp_enqueue_style('mondialrelay-style', plugin_dir_url(dirname(__FILE__)) . 'public/css/style-theme.css');
        // Load user styles
        $style = get_option('mondialrelay_style', 1);
        $style = stripslashes($style);
        wp_add_inline_style('mondialrelay-style', $style);
    }

    /**
     * Get Mondial Relay configuration parameters
     */
    public function mondialrelay_params() {
        $params = WC_MondialRelay_Order::get_params_checkout();
        foreach ($params as $param) {
            $champ = get_option( $param, 1 );
            // Texts exceptions
            if ($param == 'mondialrelay_texte_aucun' || $param == 'mondialrelay_texte_selection' || $param == 'mondialrelay_texte_erreur')
                $champ = str_replace('\\', '', $champ);
            echo '<div id="' . $param . '" style="display:none">'.$champ.'</div>';
        }
        echo '<div id="mondialrelay_debug" style="display:none">' . $this->debug . '</div>';
    }

    /**
     * Creation Fancybox
     */
    public function mondial_relay_fancybox() {
        $texte = get_option( 'mondialrelay_texte_choix', 1 );
        echo '<div id="fancybox_mondial_relay" style="display: none;"><div id="widget_mondial_relay"></div><div><a href="#" id="choisir_point_mondial_relay" class="bouton-mondial-relay">' . $texte . '</a></div></div>';
    }

    /**
     * Create Mondial Relay parameters
     * @param  object $checkout
     */
    public function my_custom_checkout_field($checkout) {
        echo '<div style="display:none"><h2>';
        _e('Infos Mondial Relay', 'woocommerce-mondialrelay');
        echo '</h2>';
        woocommerce_form_field( 'id_mondial_relay', array(
            'type'          => 'textarea',
            'label'         => __('ID Point Mondial Relay', 'woocommerce-mondialrelay'),
            'placeholder'       => '',
        ), $checkout->get_value( 'id_mondial_relay' ));
        woocommerce_form_field( 'infos_mondial_relay', array(
            'type'          => 'textarea',
            'label'         => __('Infos Point Mondial relay', 'woocommerce-mondialrelay'),
            'placeholder'       => '',
        ), $checkout->get_value( 'infos_mondial_relay' ));
        echo '</div>';
    }


}
