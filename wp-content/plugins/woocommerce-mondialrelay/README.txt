woocommerce-mondialrelay
========================

Mondial Relay plugin for WooCommerce

Contributors: Clément Barbaza

Tags: woocommerce mondialrelay

Description
--------------

Mondial Relay plugin for WooCommerce

Installation
--------------

- Upload the entire 'woocommerce-mondialrelay' folder to the '/wp-content/plugins/' directory
- Activate the plugin through the 'Plugins' menu in WordPress

**More on http://www.mondialrelay-woocommerce.com**