<?php

/**
 * Fired when the plugin is uninstalled.
 *
 *
 * @since      1.0.0
 * @package    WC_MondialRelay
 * @subpackage WC_MondialRelay/includes
 * @author     Clément Barbaza <contact@mondialrelay-woocommerce.com>
 */

// If uninstall not called from WordPress, then exit.

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

$champs = array(
	'mondialrelay_code_client',
	'mondialrelay_cle_privee',
	'mondialrelay_mode_colis',
	'mondialrelay_mode_livraison',
	'mondialrelay_assurance',
	'mondialrelay_id_livraison',
	'mondialrelay_pays_livraison',
	'mondialrelay_code_postal',
	'mondialrelay_geolocalisation',
	'mondialrelay_pays_autorises',
	'mondialrelay_ssl',
	'mondialrelay_map',
	'mondialrelay_street_view',
	'mondialrelay_zoom',
	'mondialrelay_search_delay',
	'mondialrelay_search_far',
	'mondialrelay_nb_results',
	'mondialrelay_style',
	'mondialrelay_mode_recherche',
	'mondialrelay_texte_selection',
	'mondialrelay_texte_aucun',
	'mondialrelay_texte_choix',
	'mondialrelay_texte_erreur',
	'mondialrelay_email_expedition_sujet',
	'mondialrelay_email_expedition_message',
	'mondialrelay_debug',
	'mondialrelay_menu_position',
	'mondialrelay_vendeur_adresse1',
	'mondialrelay_vendeur_adresse2',
	'mondialrelay_vendeur_adresse3',
	'mondialrelay_vendeur_cp',
	'mondialrelay_vendeur_ville',
	'mondialrelay_vendeur_pays',
	'mondialrelay_vendeur_tel',
	'mondialrelay_vendeur_email',
	'mondialrelay_parcelshop_picker'
);

foreach($champs as $champ) {
	delete_option($champ);
	// For site options in multisite
	delete_site_option($champ);
}